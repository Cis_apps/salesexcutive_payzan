package calibrage.payzanagent.networkservice;


import android.content.Context;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import calibrage.payzanagent.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceFactory  {

    /**
     * Creates a retrofit service from an arbitrary class (clazz)
     *
     * @param clazz Java interface of the retrofit service
     * @return retrofit service with defined endpoint
     */
    public static <T> T createRetrofitService(Context context, final Class<T> clazz) {
        Retrofit retrofit = new Retrofit.Builder()
                 .baseUrl(BuildConfig.LOCAL_URL)
                .client(getHttpClient(context))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        Log.d("createRetrofitService","BASE URL"+BuildConfig.LOCAL_URL);
        return retrofit.create(clazz);
    }

    private static OkHttpClient getHttpClient(final Context context) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(90, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor();
            logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            //logInterceptor.add(logInterceptor);
            httpClient.addInterceptor(logInterceptor);
       CustomResponseInterceptor  customResponseInterceptor = new CustomResponseInterceptor(context);

            httpClient.addInterceptor(customResponseInterceptor).build();
        }

        return httpClient.build();
    }

}
