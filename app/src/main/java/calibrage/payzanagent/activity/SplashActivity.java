package calibrage.payzanagent.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;

import calibrage.payzanagent.R;
import calibrage.payzanagent.commonUtil.CommonUtil;
import calibrage.payzanagent.fragments.BaseFragment;
import calibrage.payzanagent.fragments.LoginFragment;
import calibrage.payzanagent.utils.SharedPrefsData;


public class SplashActivity extends AppCompatActivity {
    public ImageView splashLogo;

    private final int SPLASH_DISPLAY_LENGTH = 5000;


   // private ProgressBar progressBar;
    Handler handler = new Handler();
    Runnable runnable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        splashLogo = (ImageView) findViewById(R.id.splash_logo);
     //   progressBar = (ProgressBar) findViewById(R.id.progressBar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !CommonUtil.areAllPermissionsAllowed(this, CommonUtil.PERMISSIONS_REQUIRED)) {
            ActivityCompat.requestPermissions(this, CommonUtil.PERMISSIONS_REQUIRED, CommonUtil.PERMISSION_CODE);
        }
        /*workingfine left_to_middle*/
       /* Animation anim = AnimationUtils.loadAnimation(this, R.anim.anim_left_to_middle);
        anim.setInterpolator((new AccelerateDecelerateInterpolator()));
        anim.setFillAfter(true);
        splashLogo.setAnimation(anim);*/
       /* Animation animation1 = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.pop_up);
        progressBar.startAnimation(animation1);*/
        runnable = new Runnable() {
            @Override
            public void run() {
               /* Intent intent = new Intent(SplashActivity.this, ChooseLanguageActivity.class);
              //  intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                startActivity(intent);
                finish();*/

               /* Intent mainIntent = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(mainIntent);
                finish();*/

                if (SharedPrefsData.getInstance(getApplicationContext()).getUserName(getApplicationContext()) != null && !SharedPrefsData.getInstance(getApplicationContext()).getUserName(getApplicationContext()).equalsIgnoreCase("")) {
                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                } else {
                    Intent intent = new Intent(getApplicationContext(), ChooseLanguageActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    startActivity(intent);
                }
                finish();
            }
        };
        handler.postDelayed(runnable, SPLASH_DISPLAY_LENGTH);

    }
       /* runnable = new Runnable() {
            @Override
            public void run() {

                Intent mainIntent = new Intent(SplashActivity.this,ChooseLanguageActivity.class);
                startActivity(mainIntent);
                finish();

                *//*  Intent mainIntent = new Intent(SplashActivity.this,
                    LoginActivity.class);
            SplashActivity.this.startActivity(mainIntent);
            SplashActivity.this.finish();
            handler.removeCallbacks(this);*//*

            }
        };
        handler.postDelayed(runnable, SPLASH_DISPLAY_LENGTH);*/




    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handler.removeCallbacks(runnable);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CommonUtil.PERMISSION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }
                break;
        }
    }


}

