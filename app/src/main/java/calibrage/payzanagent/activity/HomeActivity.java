package calibrage.payzanagent.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;

import calibrage.payzanagent.R;
import calibrage.payzanagent.fragments.BaseFragment;
import calibrage.payzanagent.fragments.LoginFragment;
import calibrage.payzanagent.fragments.MainFragment;
import calibrage.payzanagent.model.RefreshResponseModel;
import calibrage.payzanagent.model.RefreshTokenModel;
import calibrage.payzanagent.networkservice.MyServices;
import calibrage.payzanagent.networkservice.RefreshToken;
import calibrage.payzanagent.networkservice.ServiceFactory;
import calibrage.payzanagent.utils.CommonConstants;
import calibrage.payzanagent.utils.CommonUtil;
import calibrage.payzanagent.utils.MyCounter;
import calibrage.payzanagent.utils.SharedPrefsData;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static calibrage.payzanagent.commonUtil.CommonUtil.updateResources;
import static calibrage.payzanagent.utils.CommonConstants.CLIENT_ID;
import static calibrage.payzanagent.utils.CommonConstants.CLIENT_SECRET;

public class HomeActivity extends BaseActivity {
    Fragment fragment;
    FragmentManager fragmentManager;
    private int isLogin = 1;
    int val = 0;
    Dialog dialog;
    RefreshToken broadCastReceiver = new RefreshToken();
    public static Toolbar toolbar;
    private Subscription refrshSubscription;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        if (SharedPrefsData.getInstance(this).getStringFromSharedPrefs("RefreshToken") != null) {
            getRefreshToken();
        }
        val = SharedPrefsData.getInstance(HomeActivity.this).getIntFromSharedPrefs(CommonConstants.ISLOGIN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        setupToolbar();
        fragmentManager = getSupportFragmentManager();
        if (val == CommonConstants.Login) {
            ReplcaFragment(new MainFragment());

        } else {
            ReplcaFragment(new LoginFragment());
        }
    }

    private void getRefreshToken() {
        if (CommonUtil.isNetworkAvailable(HomeActivity.this)) {
            JsonObject object = getRefreshObject();
            MyServices service = ServiceFactory.createRetrofitService(HomeActivity.this, MyServices.class);
            refrshSubscription = service.getRefreshToken(object)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<RefreshResponseModel>() {
                        @Override
                        public void onCompleted() {
                            //   Toast.makeText(context, "check", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (e instanceof HttpException) {
                                ((HttpException) e).code();
                                ((HttpException) e).message();
                                ((HttpException) e).response().errorBody();
                                try {
                                    ((HttpException) e).response().errorBody().string();
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                                e.printStackTrace();
                            }
                            //Toast.makeText(HomeActivity.this, getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onNext(RefreshResponseModel refreshResponseModel) {
                            SharedPrefsData.getInstance(HomeActivity.this).updateStringValue(HomeActivity.this, "Token", refreshResponseModel.getResult().getTokenType() + " " + refreshResponseModel.getResult().getAccessToken());
                            SharedPrefsData.getInstance(HomeActivity.this).updateStringValue(HomeActivity.this, "RefreshToken", refreshResponseModel.getResult().getRefreshToken());
                            //Toast.makeText(HomeActivity.this, getString(R.string.success_response), Toast.LENGTH_SHORT).show();
                            if (CommonUtil.timer != null)
                                CommonUtil.timer.cancel();
                            CommonUtil.timer = new MyCounter(refreshResponseModel.getResult().getExpiresIn() * 1000, 1000, HomeActivity.this);
                            CommonUtil.timer.start();
                        }
                    });
        }
    }

    private JsonObject getRefreshObject() {
        RefreshTokenModel refreshTokenModel = new RefreshTokenModel();
        refreshTokenModel.setClientId(CLIENT_ID);
        refreshTokenModel.setClientSecret(CLIENT_SECRET);
        refreshTokenModel.setRefreshToken(SharedPrefsData.getInstance(HomeActivity.this).getStringFromSharedPrefs("RefreshToken"));

        return new Gson().toJsonTree(refreshTokenModel)
                .getAsJsonObject();
    }

    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_stat_arrow_back);
        //toolbar.setTitle(SharedPrefsData.getInstance(getApplicationContext()).getUserName(getApplicationContext()));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logout_menu, menu);
        return true;


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_logout) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            dialog = new Dialog(HomeActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            dialog.setContentView(R.layout.alert_dialouge_logout);
            Button ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
            Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
            ok_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    logout();
                }
            });
            cancel_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });


            dialog.show();

        } else if (item.getItemId() == android.R.id.home) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                getSupportFragmentManager().popBackStackImmediate();
                return true;
            }
        }


        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        toolbar.setTitle("Login");
        toolbar.setSubtitle("");
        SharedPrefsData.getInstance(this).ClearData(this);
        updateResources(this, "en-US");
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); ++i) {
            getSupportFragmentManager().popBackStack();
        }

        getSupportFragmentManager().beginTransaction().replace(BaseFragment.MAIN_CONTAINER, new LoginFragment()).commit();
        dialog.dismiss();

    }


    public void ReplcaFragment(android.support.v4.app.Fragment fragment) {
        fragmentManager.beginTransaction().add(R.id.content_frame, fragment).commit();
    }

    @Override
    public void onBackPressed() {
       /* this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM,
                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);*/

        if(HomeActivity.this.getCurrentFocus() != null)
        {
            InputMethodManager inputManager =
                    (InputMethodManager) getApplicationContext().
                            getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(
                    this.getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }

        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            FragmentManager fragmentManager = getSupportFragmentManager();
            // this will clear the back stack and displays no animation on the screen
            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            final Dialog dialog = new Dialog(HomeActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            dialog.setContentView(R.layout.alert_dialouge_home);
            Button ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
            Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
            ok_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    startActivity(intent);
                    finish();
                }
            });
            cancel_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });


            dialog.show();

        }
    }

}
