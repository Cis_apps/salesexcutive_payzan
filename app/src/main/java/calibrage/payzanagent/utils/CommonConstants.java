package calibrage.payzanagent.utils;


public class CommonConstants {

    // ---------- MASTER DATA CONSTANTS-------------------//

    public static String SERVICE_PROVIDER_ID_POSTPAID = "8";
    public static String SERVICE_PROVIDER_ID_PREPAID = "7";
    public static String BUSINESS_CATEGORY_ID = "3";
    public static String TITLE_ID = "4";
    public static String GENDER_ID = "5";
    public static String PERSONALID_CATEGORY_ID = "7";
    public static String FINANCIALID_CATEGORY_ID = "8";
    public static String BANK_CATEGORY_ID = "1";
    public static String BANK_ID = null;
    public static String STATUSTYPE_ID_NEW = "43";
    public static String STATUSTYPE_ID_IN_PROGRESS = "44";
    public static String STATUSTYPE_ID_SUBMIT_FOR_REVIEW = "45";
    public static String STATUSTYPE_ID_APPROVED = "46";
    public static String STATUSTYPE_ID_REJECTED = "47";
    public static String STATUSTYPE_ID_HOLD = "48";
    public static String STATES_ID = "null";
    public static String PROVINCE_NAME = "null";
    public static String FILE_TYPE_ID_DOCUMENTS = "22";
    public static String FILE_TYPE_ID_IMAGES = "23";
    public static String GENDER_TYPE_MALE = "20";
    public static String GENDER_TYPE_FEMALE = "21";

    public static String AGENT_ID = " ";
    public static boolean Is_Update = false;
    public static boolean Is_New_Agent_Request = false;
    public static String AGENT_REQUEST_ID = "";

    //---------------User Login---------------------//
    public static int Login =1;
    public static int NotLogin =0;
    public static String ISLOGIN ="";

    public static String MOBILE_NUMBER ="";


    public static String USERID = " ";
    public static String USER_NAME = " ";
    public static String WALLETID = "";
    public static String WALLETMONEY = "0";
    public static String PACKAGE = "calibrage.payzanagent";
    public static String BUNDLE_REQUEST = PACKAGE + "-BOOKING";

    public static final String CLIENT_ID = "payzan.mobile";
    public static final String CLIENT_SECRET = "PayZan!@";
    public static final String SCOPE = "offline_access";
  /*  public static final String CLIENT_ID = "payzan.web";
    public static final String CLIENT_SECRET = "PayZan!@";
    public static final String SCOPE = "api1";*/

}
