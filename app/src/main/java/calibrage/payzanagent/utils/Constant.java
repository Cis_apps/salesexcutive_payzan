package calibrage.payzanagent.utils;


public interface Constant {
 String NoConnectionError = "No connectivity: We are having trouble with the internet. Please check your connection.";
    String AuthFailureError = "Authentication Failure";
    String NetworkError = "The connection is weak. Please try again later.";
    String ParseError = "Parse Error";
    String ServerError = "Server Error";
    String TimeoutError = "The connection is weak. Please try again later.";


}

