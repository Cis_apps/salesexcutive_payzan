package calibrage.payzanagent.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;

import calibrage.payzanagent.R;
import calibrage.payzanagent.adapter.ApprovedAgentsAdapter;
import calibrage.payzanagent.model.InProgressPostModel;
import calibrage.payzanagent.model.InProgressResponseModel;
import calibrage.payzanagent.networkservice.MyServices;
import calibrage.payzanagent.networkservice.ServiceFactory;
import calibrage.payzanagent.utils.CommonConstants;
import calibrage.payzanagent.utils.EndlessRecyclerOnScrollListener;
import calibrage.payzanagent.utils.SharedPrefsData;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ApprovedAgentsFragment extends BaseFragment {

    public static final String TAG = ApprovedAgentsFragment.class.getSimpleName();

    View view;
    private RecyclerView recyclerView;
    FragmentManager fragmentManager;
    private Context context;
    TextView noRecords;
    private Subscription operatorSubscription;
    private ArrayList<InProgressResponseModel.Result.AgentRequestDetail> listResults = new ArrayList<>();
    private InProgressResponseModel agentRequestModelBundle;
    public static Toolbar toolbar;
    private EditText search;
    InProgressPostModel inprogressInfo;
    ImageView imageView;
    private String searchStr = "";
    int Index = 0, pazeSize = 10, totalItemCount, lastVisibleItem;
    LinearLayoutManager layoutManager;
    ApprovedAgentsAdapter approvedAgentsAdapter;
    private boolean loading = false;
    private SwipeRefreshLayout swipeToRefresh;
    int CurrentCount = 10;

    public ApprovedAgentsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_approved_agents, container, false);
        inprogressInfo = new InProgressPostModel();
        context = this.getActivity();
        hideKeyboard(context);
        recyclerView = (RecyclerView) view.findViewById(R.id.recylerview_card_approved);
        noRecords = (TextView) view.findViewById(R.id.no_records);
        imageView = (ImageView) view.findViewById(R.id.iv_clear);
        search = (EditText) view.findViewById(R.id.search);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!search.getText().toString().trim().equalsIgnoreCase("")) {
                    search.setText("");
                    listResults.clear();
                    postList("", 0, 10, true);
                }

                // getRequest(CommonConstants.USERID + "/" + CommonConstants.STATUSTYPE_ID_APPROVED, null);
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        fragmentManager = getActivity().getSupportFragmentManager();
        if (isOnline(getActivity())) {
            postList(searchStr, Index, pazeSize, true);

            //getRequest(CommonConstants.USERID + "/" + CommonConstants.STATUSTYPE_ID_APPROVED, searchStr);
        } else {
            showToast(getActivity(), getString(R.string.no_internet));
        }
        addTextListener();

        swipeToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                listResults.clear();
                if (!listResults.isEmpty())
                    listResults.clear();//The list for update recycle view
                approvedAgentsAdapter.notifyDataSetChanged();
                Index = 0;
                CurrentCount = 10;
                EndlessRecyclerOnScrollListener.current_page = 1;
                EndlessRecyclerOnScrollListener.loading = true;
                EndlessRecyclerOnScrollListener.previousTotal = 0;
                postList(searchStr, Index, pazeSize, true);
            }


        });
        recyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener(
                layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                // do something...
                int count = 0;
                Index = Index + 1;
                EndlessRecyclerOnScrollListener.current_page = 1;
                EndlessRecyclerOnScrollListener.loading = true;
                EndlessRecyclerOnScrollListener.previousTotal = 0;
                if (CurrentCount == pazeSize) {
                    postList("", Index, pazeSize, false);
                }

            }

        });

        return view;
    }


    private void postList(String searchStr, Integer index, Integer pageSize, final Boolean loaData) {
        showDialog(getActivity(), getString(R.string.authenticating_alert));
        JsonObject object = inprogressDetails(searchStr, index, pageSize);
        MyServices service = ServiceFactory.createRetrofitService(getActivity(), MyServices.class);
        operatorSubscription = service.postInprogress(object)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<InProgressResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        Toast.makeText(getActivity(), getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(InProgressResponseModel inProgressResponseModel) {
                        hideDialog();
                        CurrentCount = inProgressResponseModel.getResult().getPageSize();
                        if (inProgressResponseModel.getIsSuccess()) {
                            swipeToRefresh.setRefreshing(false);
                            Index = inProgressResponseModel.getResult().getPageIndex();
                            for (int i = 0; i < inProgressResponseModel.getResult().getAgentRequestDetails().size(); i++) {
                                listResults.add(inProgressResponseModel.getResult().getAgentRequestDetails().get(i));
                            }
                            if (listResults.isEmpty()) {
                                noRecords.setVisibility(View.VISIBLE);

                            } else {
                                noRecords.setVisibility(View.GONE);
                            }
                            if (loaData) {
                                approvedAgentsAdapter = new ApprovedAgentsAdapter(context, listResults);
                                recyclerView.setAdapter(approvedAgentsAdapter);
                                recyclerView.setHasFixedSize(true);

                            } else {
                                approvedAgentsAdapter.notifyDataSetChanged();
                            }
                            agentRequestModelBundle = inProgressResponseModel;

                        } else {
                            showToast(context, inProgressResponseModel.getEndUserMessage());
                        }
                    }

                });
    }

    private JsonObject inprogressDetails(String searchStr, Integer index, Integer pageSize) {
        inprogressInfo.setUserId(SharedPrefsData.getInstance(context).getUserId(context));
        inprogressInfo.setStatusTypeIds(CommonConstants.STATUSTYPE_ID_APPROVED);
        if (searchStr != null && searchStr.equalsIgnoreCase("")) {
            inprogressInfo.setSearchItem(searchStr);
            inprogressInfo.setIndex(index);
            inprogressInfo.setPageSize(pageSize);
        } else {
            inprogressInfo.setSearchItem(searchStr);
            inprogressInfo.setIndex(null);
            inprogressInfo.setPageSize(null);
        }
        Log.e("inprogressInfo", "" + new Gson().toJsonTree(inprogressInfo)
                .getAsJsonObject());
        return new Gson().toJsonTree(inprogressInfo)
                .getAsJsonObject();
    }

    private void addTextListener() {
        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (v.getText().toString().equalsIgnoreCase("") || v.getText().toString().equalsIgnoreCase(null)) {
                        listResults.clear();
                        postList("", null, null, false);
                    } else {
                        listResults.clear();
                        postList(v.getText().toString().trim(), null, null, false);
                    }
                    return true;
                }
                return false;
            }
        });
    }

    public void ReplcaFragment(android.support.v4.app.Fragment fragment) {
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }


}
