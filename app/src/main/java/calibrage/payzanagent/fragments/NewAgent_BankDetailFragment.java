package calibrage.payzanagent.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;

import calibrage.payzanagent.BuildConfig;
import calibrage.payzanagent.R;
import calibrage.payzanagent.model.AddAgent;
import calibrage.payzanagent.model.AgentBankInfo;
import calibrage.payzanagent.model.BankInfoResponseModel;
import calibrage.payzanagent.model.Branch;
import calibrage.payzanagent.model.BusinessCategoryModel;
import calibrage.payzanagent.model.GetBankInfoModel;
import calibrage.payzanagent.networkservice.ApiConstants;
import calibrage.payzanagent.networkservice.MyServices;
import calibrage.payzanagent.networkservice.ServiceFactory;
import calibrage.payzanagent.utils.CommonConstants;
import calibrage.payzanagent.utils.SharedPrefsData;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Admin on 2/13/2018.
 */

public class NewAgent_BankDetailFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = NewAgent_BankDetailFragment.class.getSimpleName();


    View view;
    private Button btnContinue;
    FragmentManager fragmentManager;
    private Context context;

    private Subscription operatorSubscription;
    Spinner spinnerCustom_bank;
    Spinner spinnerCustom_brach;
    int bankId;
    private Button personalButton, bankButton, idButton, documentButton, btnCancel;
    EditText accountName, accountNo, shiftCode;
    ArrayList<String> bankArrayList = new ArrayList<String>();
    ArrayList<String> branchArrayList = new ArrayList<String>();
    private ArrayList<Branch.ListResult> branchListResults = new ArrayList<>();
    private ArrayList<BusinessCategoryModel.ListResult> bankListResults = new ArrayList<>();
    public static Toolbar toolbar;
    private AddAgent addAgent;
    ScrollView scrollview;
    private String straccountname, straccountno, strshiftcode, currentDatetime, strCreatedby;
    private AgentBankInfo agentBankInfo;
    // BankDetailFragment.CustomSpinnerAdapter customSpinnerAdapter;
    private int id;
    ArrayAdapter bankArrayAdapter, branchArrayAdapter;


    public NewAgent_BankDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_newagent_bank_detail, container, false);
        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                // hideKeyboard(context);
                return true;
            }
        });
        if(getActivity().getCurrentFocus() != null)
        {
            InputMethodManager inputManager =
                    (InputMethodManager) getActivity().
                            getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(
                    getActivity().getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
        context = this.getActivity();
        currentDatetime = SharedPrefsData.getInstance(context).getStringFromSharedPrefs("datetime");
        agentBankInfo = new AgentBankInfo();
        initCustomSpinner_bank();
        personalButton = (Button) view.findViewById(R.id.btn_personal);
        btnCancel = (Button) view.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);
        bankButton = (Button) view.findViewById(R.id.btn_bank);
        idButton = (Button) view.findViewById(R.id.btn_id);
        documentButton = (Button) view.findViewById(R.id.btn_doc);
        bankButton.setOnClickListener(this);
        idButton.setOnClickListener(this);
        scrollview = view.findViewById(R.id.scrollview);
        scrollview.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if (event != null && event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    InputMethodManager imm = ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE));
                    boolean isKeyboardUp = imm.isAcceptingText();

                    if (isKeyboardUp)
                    {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
                return false;
            }
        });
        if (isOnline(getActivity())) {
            getRequestBank(CommonConstants.BANK_CATEGORY_ID);
        } else {
            showToast(getActivity(), getString(R.string.no_internet));
        }

        initCustomSpinner_branch();
        btnContinue = (Button) view.findViewById(R.id.btn_continue);
        accountName = (EditText) view.findViewById(R.id.txt_accountholdername);
        accountNo = (EditText) view.findViewById(R.id.txt_accountno);
        shiftCode = (EditText) view.findViewById(R.id.txt_swift_code);

    /*    accountNo.setFilters(new InputFilter[]{EMOJI_FILTER});
        accountName.setFilters(new InputFilter[]{EMOJI_FILTER});
        shiftCode.setFilters(new InputFilter[]{EMOJI_FILTER});*/


        fragmentManager = getActivity().getSupportFragmentManager();
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnline(getActivity())) {
                    if (isValidateUi()) {
                            postBankInfo();


                        hideKeyboard(context);
                    }
                } else {
                    showToast(getActivity(), getString(R.string.no_internet));
                }

            }
        });

        return view;
    }


    private JsonObject agentBankDetails(boolean isUpdate) {
        Log.d(TAG, "agentBankDetails: "+id);
        agentBankInfo.setIsActive(true);
        agentBankInfo.setAccountHolderName(straccountname);
        agentBankInfo.setAccountNumber(straccountno);
        agentBankInfo.setBankId("" + branchListResults.get(spinnerCustom_brach.getSelectedItemPosition()-1).getId());
        agentBankInfo.setAgentId("" + CommonConstants.AGENT_ID);
        agentBankInfo.setId(0);

        return new Gson().toJsonTree(agentBankInfo)
                .getAsJsonObject();
    }


    private void postBankInfo() {
        showDialog(getActivity(), getString(R.string.authenticating_alert));
        JsonObject object = agentBankDetails(false);
        Log.d(TAG, "postBankInfo: "+agentBankDetails(false).toString());
        MyServices service = ServiceFactory.createRetrofitService(getActivity(), MyServices.class);
        operatorSubscription = service.postBankInfo(object)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BankInfoResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        //Toast.makeText(getActivity(), getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(BankInfoResponseModel bankInfoResponseModel) {
                        hideDialog();
                        if (bankInfoResponseModel.getIsSuccess()) {
                            showToast(context, bankInfoResponseModel.getEndUserMessage());
                            replaceFragment(getActivity(), MAIN_CONTAINER, new NewAgent_IdProofFragment(), TAG, NewAgent_IdProofFragment.TAG);

                        } else {
                            showToast(context, bankInfoResponseModel.getEndUserMessage());
                        }

                    }
                });

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        getRequestBank(CommonConstants.BANK_CATEGORY_ID);
    }

    private boolean isValidateUi() {
        boolean status = true;
        straccountname = accountName.getText().toString().trim();
        straccountno = accountNo.getText().toString();
        strshiftcode = shiftCode.getText().toString();


        if (straccountname.isEmpty() ) {
            status = false;
            accountName.setError(getString(R.string.accountHolder_Name_is_required));
            accountName.requestFocusFromTouch();
        }else if (accountName.getText().length() < 3) {
            status = false;
            accountName.setError(getString(R.string.Name_is_required));
            accountName.requestFocusFromTouch();
        } else if (straccountno.isEmpty()||accountNo.getText().length()<1) {
            status = false;
            accountNo.setError(getString(R.string.accountNumber_is_required));
            accountNo.requestFocusFromTouch();
        } else if (spinnerCustom_bank.getSelectedItemPosition() ==0) {
            status = false;
            Toast.makeText(context, getString(R.string.bank_is_required), Toast.LENGTH_SHORT).show();
        }  else if (bankListResults.isEmpty()) {
            status = false;
            Toast.makeText(context, getString(R.string.no_results_found), Toast.LENGTH_SHORT).show();
        }else if (spinnerCustom_brach.getSelectedItemPosition() == 0) {
            status = false;
            Toast.makeText(context, getString(R.string.branch_is_required), Toast.LENGTH_SHORT).show();
        } else if (branchListResults.isEmpty()) {
            status = false;
            Toast.makeText(context, getString(R.string.no_results_found), Toast.LENGTH_SHORT).show();
        } else if (strshiftcode.isEmpty()) {
            status = false;
            shiftCode.setError(getString(R.string.shiftcode_is_required));
            /*shiftCode.requestFocusFromTouch();*/
        }
        return status;
    }

    private void getRequestBranch(String providerType) {
        showDialog(getActivity(), getString(R.string.authenticating_alert));
        String data = BuildConfig.LOCAL_URL + ApiConstants.BRANCH_REQUESTS + providerType;
        MyServices service = ServiceFactory.createRetrofitService(context, MyServices.class);
        operatorSubscription = service.getBranchRequest(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Branch>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        //Toast.makeText(context, getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(Branch branch) {
                        hideDialog();
                        branchListResults = (ArrayList<Branch.ListResult>) branch.getListResult();

                        branchArrayList = new ArrayList();
                        if (branch.getListResult().size() > 0 && !branchListResults.isEmpty()) {

                            branchArrayList.add("-- Select Branch --");
                            for (int i = 0; i < branch.getListResult().size(); i++) {
                                branchArrayList.add(branch.getListResult().get(i).getBranchName());
                            }
                        }
                      else {
                            branchArrayList.clear();
                           // branchArrayAdapter.notifyDataSetChanged();
                            shiftCode.setText("");
                        }

                        branchArrayAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, branchArrayList);
                        branchArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner
                        spinnerCustom_brach.setAdapter(branchArrayAdapter);


                    }

                });
    }

    private void getRequestBank(String providerType) {
        MyServices service = ServiceFactory.createRetrofitService(context, MyServices.class);
        operatorSubscription = service.getBusinessRequest(ApiConstants.BUSINESS_CAT_REQUESTS + providerType)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BusinessCategoryModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        //Toast.makeText(context, getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(BusinessCategoryModel businessCategoryModel) {
                        hideDialog();
                        bankListResults = (ArrayList<BusinessCategoryModel.ListResult>) businessCategoryModel.getListResult();
                            if (!bankListResults.isEmpty()) {
                                bankArrayList.add("--Select Bank--");
                                for (int i = 0; i < businessCategoryModel.getListResult().size(); i++) {
                                    bankArrayList.add(businessCategoryModel.getListResult().get(i).getDescription());

                                }
                            }


                        bankArrayAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, bankArrayList);
                        bankArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner

                        spinnerCustom_bank.setAdapter(bankArrayAdapter);

                    }

                });


    }



    private void initCustomSpinner_branch() {
        spinnerCustom_brach = (Spinner) view.findViewById(R.id.spinner_branch);
        spinnerCustom_brach.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event != null && event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    InputMethodManager imm = ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE));
                    boolean isKeyboardUp = imm.isAcceptingText();

                    if (isKeyboardUp)
                    {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
                return false;
            }
        }) ;
        spinnerCustom_brach.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {

                //    String item = parent.getItemAtPosition(position).toString();
                if (!branchListResults.isEmpty()&& spinnerCustom_brach.getSelectedItemPosition() != 0) {
                    shiftCode.setText(branchListResults.get(i-1).getSwiftCode());
                }else {
                    shiftCode.setText("");
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    private void initCustomSpinner_bank() {

        spinnerCustom_bank = (Spinner) view.findViewById(R.id.spinner_bank);
        //  spinnerCustom_bank.setPrompt("testing");
        spinnerCustom_bank.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event != null && event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    InputMethodManager imm = ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE));
                    boolean isKeyboardUp = imm.isAcceptingText();

                    if (isKeyboardUp)
                    {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
                return false;
            }
        }) ;
        spinnerCustom_bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {

                //  bankId = (int) parent.getSelectedItemId();


                if (isOnline(getActivity())) {
                    if (!bankListResults.isEmpty()&&spinnerCustom_bank.getSelectedItemPosition() != 0 ) {
                        bankId = bankListResults.get(i-1).getId();
                        getRequestBranch(String.valueOf(bankId));

                        shiftCode.setText(" ");
                    }else {
                        spinnerCustom_brach.setSelection(0);
                        shiftCode.setText(" ");
                    }
                } else {
                    showToast(getActivity(), getString(R.string.no_internet));
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_bank:
                showToast(getActivity(), getString(R.string.please_Fill_The_Bank_Details));
                break;
            case R.id.btn_id:
                accountName.setError(null);
                accountNo.setError(null);
                shiftCode.setError(null);
                if(getActivity().getCurrentFocus() != null)
                {
                    InputMethodManager inputManager =
                            (InputMethodManager) getActivity().
                                    getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(
                            getActivity().getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }

                if (isValidateUi()) {
                    postBankInfo();


                    hideKeyboard(context);
                }

                break;
            case R.id.btn_cancel:
                replaceFragment(getActivity(), MAIN_CONTAINER, new MainFragment(), TAG, MainFragment.TAG);
                break;

        }
    }


    public void ReplcaFragment(android.support.v4.app.Fragment fragment) {
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }


}
