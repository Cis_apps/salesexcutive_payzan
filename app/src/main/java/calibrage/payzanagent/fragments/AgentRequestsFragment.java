package calibrage.payzanagent.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;

import calibrage.payzanagent.R;
import calibrage.payzanagent.adapter.AgentRequetAdapter;
import calibrage.payzanagent.interfaces.RequestClickListiner;
import calibrage.payzanagent.model.InProgressPostModel;
import calibrage.payzanagent.model.InProgressResponseModel;
import calibrage.payzanagent.model.UpdateAgentRequestModel;
import calibrage.payzanagent.model.UpdateAgentRequestResponceModel;
import calibrage.payzanagent.networkservice.MyServices;
import calibrage.payzanagent.networkservice.ServiceFactory;
import calibrage.payzanagent.utils.CommonConstants;
import calibrage.payzanagent.utils.EndlessRecyclerOnScrollListener;
import calibrage.payzanagent.utils.SharedPrefsData;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AgentRequestsFragment extends BaseFragment implements RequestClickListiner {

    public static final String TAG = AgentRequestsFragment.class.getSimpleName();

    View view;
    private RecyclerView recyclerView;
    FragmentManager fragmentManager;
    private Context context;
    private Subscription operatorSubscription;
    private Subscription mRegisterSubscription;
    private String stComment, currentDatetime;
    private int position;
    TextView noRecords;
    private EditText search;
    private
    InProgressPostModel inprogressInfo;
    ImageView imageView;
    private String searchStr = "";
    private boolean isPickorHold;
    private ArrayList<InProgressResponseModel.Result.AgentRequestDetail> listResults = new ArrayList<>();
    ;
    private InProgressResponseModel agentRequestModelBundle;
    public static Toolbar toolbar;
    int Index = 0, pazeSize = 10, totalItemCount, lastVisibleItem;
    LinearLayoutManager layoutManager;
    private boolean loading = false;
    private SwipeRefreshLayout swipeToRefresh;
    AgentRequetAdapter agentRequetAdapter;
    int CurrentCount = 10;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_agent_request, container, false);
        inprogressInfo = new InProgressPostModel();
        context = this.getActivity();
        hideKeyboard(context);
        imageView = (ImageView) view.findViewById(R.id.iv_clear);
        search = (EditText) view.findViewById(R.id.search);
        currentDatetime = SharedPrefsData.getInstance(context).getStringFromSharedPrefs("datetime");
        recyclerView = (RecyclerView) view.findViewById(R.id.recylerview_card);
        noRecords = (TextView) view.findViewById(R.id.no_records);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        fragmentManager = getActivity().getSupportFragmentManager();
        swipeToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

//                listResults.clear();
                if (!listResults.isEmpty())
                    listResults.clear();//The list for update recycle view
                agentRequetAdapter.notifyDataSetChanged();
                Index = 0;
                CurrentCount = 10;
                EndlessRecyclerOnScrollListener.current_page = 1;
                EndlessRecyclerOnScrollListener.loading = true;
                EndlessRecyclerOnScrollListener.previousTotal = 0;
                postList(searchStr, Index, pazeSize, true);

            }


        });


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!search.getText().toString().trim().equalsIgnoreCase("")) {
                    search.setText("");
                    listResults.clear();
                    postList("", 0, 10, true);
                }


            }
        });
        String val = SharedPrefsData.getInstance(context).getStringFromSharedPrefs("mahesh");
        if (isOnline(getActivity())) {
            postList(searchStr, Index, pazeSize, true);
        } else {
            showToast(getActivity(), getString(R.string.no_internet));
        }
        recyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener(
                layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                // do something...
                int count = 0;
                Index = Index + 1;
                EndlessRecyclerOnScrollListener.current_page = 1;
                EndlessRecyclerOnScrollListener.loading = true;
                EndlessRecyclerOnScrollListener.previousTotal = 0;
                if (CurrentCount == pazeSize) {
                    postList("", Index, pazeSize, false);
                }

            }

        });

        addTextListener();
        return view;

    }

    private void postList(final String searchStr, final Integer index, final Integer pageSize, final Boolean loaData) {

        showDialog(getActivity(), getString(R.string.authenticating_alert));
        JsonObject object = inprogressDetails(searchStr, index, pageSize);
        MyServices service = ServiceFactory.createRetrofitService(getActivity(), MyServices.class);
        operatorSubscription = service.postInprogress(object)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<InProgressResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        Toast.makeText(getActivity(), getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(InProgressResponseModel inProgressResponseModel) {
                        hideDialog();
                        Log.d(TAG, "response " + listResults.size());
                        Log.d(TAG, "response " + inProgressResponseModel.getResult().toString());
                        CurrentCount = inProgressResponseModel.getResult().getPageSize();
                      //  showToast(getActivity(), String.valueOf(listResults.size()));
                        if (inProgressResponseModel.getIsSuccess()) {
                            swipeToRefresh.setRefreshing(false);
                            Index = inProgressResponseModel.getResult().getPageIndex();
                            for (int i = 0; i < inProgressResponseModel.getResult().getAgentRequestDetails().size(); i++) {
                                listResults.add(inProgressResponseModel.getResult().getAgentRequestDetails().get(i));
                            }
                            // showToast(context, ""+listResults.size());
                            //   listResults = (ArrayList<InProgressResponseModel.Result.AgentRequestDetail>) inProgressResponseModel.getResult().getAgentRequestDetails();
                            if (listResults.isEmpty()) {
                                noRecords.setVisibility(View.VISIBLE);

                            } else {
                                noRecords.setVisibility(View.GONE);
                            }
                            if (loaData) {
                                agentRequetAdapter = new AgentRequetAdapter(context, listResults);
                                recyclerView.setAdapter(agentRequetAdapter);
                                agentRequetAdapter.setOnAdapterListener(AgentRequestsFragment.this);
                            } else {
                                agentRequetAdapter.notifyDataSetChanged();
                            }

                          //  agentRequestModelBundle = inProgressResponseModel;


                        } else {
                            showToast(context, inProgressResponseModel.getEndUserMessage());
                        }
                    }
                });
    }

    private JsonObject inprogressDetails(String searchString, Integer index, Integer pageSize) {
        inprogressInfo.setUserId(SharedPrefsData.getInstance(context).getUserId(context));
        inprogressInfo.setStatusTypeIds(CommonConstants.STATUSTYPE_ID_NEW + "," + CommonConstants.STATUSTYPE_ID_REJECTED + "," + CommonConstants.STATUSTYPE_ID_HOLD);
        if (searchString != null && searchString.equalsIgnoreCase("")) {
            inprogressInfo.setSearchItem(searchString);
            inprogressInfo.setIndex(index);
            inprogressInfo.setPageSize(pageSize);
        } else {
            inprogressInfo.setSearchItem(searchString);
            inprogressInfo.setIndex(null);
            inprogressInfo.setPageSize(null);
        }
        Log.e("inprogressInfo", "" + new Gson().toJsonTree(inprogressInfo)
                .getAsJsonObject());
        return new Gson().toJsonTree(inprogressInfo)
                .getAsJsonObject();
    }

    private void addTextListener() {
        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (v.getText().toString().equalsIgnoreCase("") || v.getText().toString().equalsIgnoreCase(null)) {
                        listResults.clear();
                        postList(" ", null, null, false);
                    } else {
                        listResults.clear();
                        postList(v.getText().toString().trim(), null, null, false);
                    }


                    return true;
                }
                return false;
            }
        });
    }


    public void ReplcaFragment(android.support.v4.app.Fragment fragment) {
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }


    @SuppressLint("ResourceType")
    @Override
    public void onAdapterClickListiner(int pos, boolean isPickorHold) {
        position = pos;
        this.isPickorHold = isPickorHold;
        final Dialog adb = new Dialog(getActivity());
        adb.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layout = inflater.inflate(R.layout.row_comment_box, null);
        adb.setContentView(layout);
        adb.getWindow().setLayout((int) getResources().getDimension(R.dimen.comments_box_width), (int) getResources().getDimension(R.dimen.comments_box_height));
        adb.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        adb.show();
        final EditText etComment = (EditText) layout.findViewById(R.id.etComment);
        final TextView user_name = layout.findViewById(R.id.user_name);
        user_name.setText(listResults.get(position).getMobileNumber());
        Button btnSubmit = (Button) layout.findViewById(R.id.btnSubmit);
        Button btnCancel = (Button) layout.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adb.dismiss();
            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(context);
                stComment = etComment.getText().toString();
                if (stComment.equalsIgnoreCase("")) {
                    showToast(getActivity(), getString(R.string.please_write_comments));

                } else {

                    submitRequest();
                    adb.dismiss();


                }
            }
        });
    }

    private void submitRequest() {
        showDialog(getActivity(), getString(R.string.authenticating_alert));
        JsonObject object = getLoginObject();
        MyServices service = ServiceFactory.createRetrofitService(getActivity(), MyServices.class);
        mRegisterSubscription = service.AgentUpdateRequest(object)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UpdateAgentRequestResponceModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        Toast.makeText(getActivity(), getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(UpdateAgentRequestResponceModel updateAgentRequestResponceModel) {
                        hideDialog();
                        if (updateAgentRequestResponceModel.getIsSuccess()) {
                            showToast(getActivity(), updateAgentRequestResponceModel.getEndUserMessage());
                        //    showToast(getActivity(), String.valueOf(listResults.size()));
                            hideKeyboard(context);
                            replaceFragment(getActivity(), MAIN_CONTAINER, new AgentRequestsFragment(), TAG, AgentRequestsFragment.TAG);
                            //postList("", 0, pazeSize, true);
                        } else {
                            showToast(context, updateAgentRequestResponceModel.getEndUserMessage());
                        }

                    }
                });
    }

    private JsonObject getLoginObject() {
        UpdateAgentRequestModel updateAgentRequestModel = new UpdateAgentRequestModel();
        updateAgentRequestModel.setAgentRequestId(listResults.get(position).getId());
        /**
         * isPick or Hold is true --->executive pick the request and if false it is vice -versa
         */
        if (isPickorHold) {
            updateAgentRequestModel.setStatusTypeId(Integer.valueOf(CommonConstants.STATUSTYPE_ID_IN_PROGRESS));
        } else {
            updateAgentRequestModel.setStatusTypeId(Integer.valueOf(CommonConstants.STATUSTYPE_ID_HOLD));
        }

        updateAgentRequestModel.setAssignToUserId(SharedPrefsData.getInstance(context).getUserId(context));
        updateAgentRequestModel.setComments(stComment);
        updateAgentRequestModel.setId(null);
        updateAgentRequestModel.setIsActive(true);
        updateAgentRequestModel.setCreatedBy(SharedPrefsData.getInstance(context).getUserId(context));
        updateAgentRequestModel.setModifiedBy(SharedPrefsData.getInstance(context).getUserId(context));
        updateAgentRequestModel.setCreated(currentDatetime);
        updateAgentRequestModel.setModified(currentDatetime);
        return new Gson().toJsonTree(updateAgentRequestModel)
                .getAsJsonObject();
    }
}
