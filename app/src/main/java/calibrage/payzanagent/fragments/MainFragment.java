package calibrage.payzanagent.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import calibrage.payzanagent.R;
import calibrage.payzanagent.activity.HomeActivity;
import calibrage.payzanagent.model.HomeModel;
import calibrage.payzanagent.networkservice.ApiConstants;
import calibrage.payzanagent.networkservice.MyServices;
import calibrage.payzanagent.networkservice.ServiceFactory;
import calibrage.payzanagent.utils.CommonConstants;
import calibrage.payzanagent.utils.SharedPrefsData;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = MainFragment.class.getSimpleName();
    View view;
    private Context context;
    private String stUsername;
    private Subscription operatorSubscription;
    String userid;
    TextView txtCount1, txtCount2, txtCount3, txtCount4, txtStatusType1, txtStatusType2, txtStatusType3, txtStatusType4;
    Button btnNewAgent, btnAgentRequest, btnToverify, btnApprovedAgents;
    private ImageView refresh;

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        hideDialog();
        setHasOptionsMenu(true);
        view = inflater.inflate(R.layout.fragment_main, container, false);
        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        context = this.getActivity();
      //  hideKeyboard(context);
        // stUsername = SharedPrefsData.getInstance(context).getStringFromSharedPrefs("username");
        stUsername = SharedPrefsData.getInstance(context).getUserName(context);
        HomeActivity.toolbar.setTitle(stUsername);
        HomeActivity.toolbar.setTitleTextColor(ContextCompat.getColor(context, R.color.white_new));

        userid = SharedPrefsData.getInstance(context).getUserId(context);

        if (isOnline(getActivity())) {
            getRequestCount(userid);
        } else {
            showToast(getActivity(), getString(R.string.no_internet));
        }
        txtCount1 = (TextView) view.findViewById(R.id.txt_count1);
        txtCount2 = (TextView) view.findViewById(R.id.txt_count2);
        txtCount3 = (TextView) view.findViewById(R.id.txt_count3);
        txtCount4 = (TextView) view.findViewById(R.id.txt_count4);
        txtStatusType1 = (TextView) view.findViewById(R.id.txt_status_type1);
        txtStatusType2 = (TextView) view.findViewById(R.id.txt_status_type2);
        txtStatusType3 = (TextView) view.findViewById(R.id.txt_status_type3);
        txtStatusType4 = (TextView) view.findViewById(R.id.txt_status_type4);
        refresh = (ImageView) view.findViewById(R.id.refresh);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RotateAnimation rotate = new RotateAnimation(0, 360,
                        Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                rotate.setDuration(1000);
                rotate.setRepeatCount(Animation.INFINITE);
                rotate.setRepeatMode(Animation.INFINITE);
                rotate.setInterpolator(new LinearInterpolator());
                refresh.startAnimation(rotate);
                if (isOnline(getActivity())) {
                    getRequestCount(userid);
                } else {
                    showToast(getActivity(), getString(R.string.no_internet));
                    refresh.clearAnimation();
                }
            }
        });



        btnNewAgent = (Button) view.findViewById(R.id.btn_newAgent);
        btnAgentRequest = (Button) view.findViewById(R.id.btn_agentRequests);
        btnToverify = (Button) view.findViewById(R.id.btn_yetToVerify);
        btnApprovedAgents = (Button) view.findViewById(R.id.btn_approvedAgents);
        btnNewAgent.setOnClickListener(this);
        btnAgentRequest.setOnClickListener(this);
        btnToverify.setOnClickListener(this);
        btnApprovedAgents.setOnClickListener(this);
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_logout);
        item.setVisible(true);
    }

    private void getRequestCount(String providerType) {
        showDialog(getActivity(), getString(R.string.authenticating_alert));
        MyServices service = ServiceFactory.createRetrofitService(context, MyServices.class);
        operatorSubscription = service.getHomeRequest(ApiConstants.HOME_REQUEST + providerType)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<HomeModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        refresh.clearAnimation();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        Toast.makeText(context, getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(HomeModel homeModel) {
                        hideDialog();
                        refresh.clearAnimation();

                       // int pos =homeModel.getResult().getStatusCounts().indexOf()
                        ArrayList<String> count = new ArrayList<>();

                        for (int i = 0; i < homeModel.getResult().getStatusCounts().size(); i++) {
                            count.add(homeModel.getResult().getStatusCounts().get(i).getStatusTypeId().toString());
                        }

                      int posNew =   count.indexOf(CommonConstants.STATUSTYPE_ID_NEW);
                      int posRej =   count.indexOf(CommonConstants.STATUSTYPE_ID_REJECTED);
                      int posHold =   count.indexOf(CommonConstants.STATUSTYPE_ID_HOLD);

                      int newCount =homeModel.getResult().getStatusCounts().get(posNew).getCount()+homeModel.getResult().getStatusCounts().get(posRej).getCount()+homeModel.getResult().getStatusCounts().get(posHold).getCount();
                        txtCount1.setText("" + newCount);
                        int posReview =  count.indexOf(CommonConstants.STATUSTYPE_ID_SUBMIT_FOR_REVIEW);
                        txtCount3.setText("" + homeModel.getResult().getStatusCounts().get(posReview).getCount());
                        int posProgress =  count.indexOf(CommonConstants.STATUSTYPE_ID_IN_PROGRESS);
                        txtCount2.setText("" + homeModel.getResult().getStatusCounts().get(posProgress).getCount());
                        int posApp =  count.indexOf(CommonConstants.STATUSTYPE_ID_APPROVED);
                        txtCount4.setText("" + homeModel.getResult().getStatusCounts().get(posApp).getCount());

                    }

                });
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_newAgent:
                replaceFragment(getActivity(), MAIN_CONTAINER, new NewAgent_RegistrationViewFragment(), TAG, NewAgent_RegistrationViewFragment.TAG);
                CommonConstants.Is_New_Agent_Request = true;
                break;
            case R.id.btn_agentRequests:
                replaceFragment(getActivity(), MAIN_CONTAINER, new AgentRequestsFragment(), TAG, AgentRequestsFragment.TAG);
                CommonConstants.Is_New_Agent_Request = false;
                break;
            case R.id.btn_yetToVerify:
                replaceFragment(getActivity(), MAIN_CONTAINER, new InProgressFragment(), TAG, InProgressFragment.TAG);
                CommonConstants.Is_New_Agent_Request = false;
                break;
            case R.id.btn_approvedAgents:
                replaceFragment(getActivity(), MAIN_CONTAINER, new ApprovedAgentsFragment(), TAG, ApprovedAgentsFragment.TAG);
                CommonConstants.Is_New_Agent_Request = false;
                break;

        }

    }


}
