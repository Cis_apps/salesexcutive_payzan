package calibrage.payzanagent.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;

import calibrage.payzanagent.BuildConfig;
import calibrage.payzanagent.R;
import calibrage.payzanagent.model.AddAgent;
import calibrage.payzanagent.model.AgentBankInfo;
import calibrage.payzanagent.model.BankInfoResponseModel;
import calibrage.payzanagent.model.Branch;
import calibrage.payzanagent.model.BusinessCategoryModel;
import calibrage.payzanagent.model.GetBankInfoModel;
import calibrage.payzanagent.networkservice.ApiConstants;
import calibrage.payzanagent.networkservice.MyServices;
import calibrage.payzanagent.networkservice.ServiceFactory;
import calibrage.payzanagent.utils.CommonConstants;
import calibrage.payzanagent.utils.SharedPrefsData;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Admin on 2/13/2018.
 */

public class Update_BankDetailFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = Update_BankDetailFragment.class.getSimpleName();

    View view;
    private Button btnContinue;
    FragmentManager fragmentManager;
    private Context context;

    private Subscription operatorSubscription;
    Spinner spinnerCustom_bank;
    Spinner spinnerCustom_brach;
    int bankId;
    private Button personalButton, bankButton, idButton, documentButton, btnCancel;
    EditText accountName, accountNo, shiftCode;
    ArrayList<String> bankArrayList = new ArrayList<String>();
    ArrayList<String> branchArrayList = new ArrayList<String>();
    private ArrayList<Branch.ListResult> branchListResults = new ArrayList<>();
    private ArrayList<BusinessCategoryModel.ListResult> bankListResults = new ArrayList<>();
    public static Toolbar toolbar;
    private AddAgent addAgent;
    ScrollView scrollview;
    private String straccountname, straccountno, strshiftcode, currentDatetime, strCreatedby;
    private AgentBankInfo agentBankInfo;

    private boolean isUpdate = false;//It is the key value to recogize whether update the bank info or post bank info
    private int id;
    private AlertDialog alertDialog;
    ArrayAdapter bankArrayAdapter, branchArrayAdapter;


    public Update_BankDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_bank_detail_update, container, false);
        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                // hideKeyboard(context);
                return true;
            }
        });
        if(getActivity().getCurrentFocus() != null)
        {
            InputMethodManager inputManager =
                    (InputMethodManager) getActivity().
                            getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(
                    getActivity().getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
        context = this.getActivity();
        currentDatetime = SharedPrefsData.getInstance(context).getStringFromSharedPrefs("datetime");
        agentBankInfo = new AgentBankInfo();
        initCustomSpinner_bank();
        personalButton = (Button) view.findViewById(R.id.btn_personal);
        btnCancel = (Button) view.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);
        bankButton = (Button) view.findViewById(R.id.btn_bank);
        idButton = (Button) view.findViewById(R.id.btn_id);
        documentButton = (Button) view.findViewById(R.id.btn_doc);
        bankButton.setOnClickListener(this);
        personalButton.setOnClickListener(this);
        idButton.setOnClickListener(this);
        scrollview = view.findViewById(R.id.scrollview);
        scrollview.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if (event != null && event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    InputMethodManager imm = ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE));
                    boolean isKeyboardUp = imm.isAcceptingText();

                    if (isKeyboardUp)
                    {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
                return false;
            }
        });
        if (isOnline(getActivity())) {
            getRequestBank(CommonConstants.BANK_CATEGORY_ID);
        } else {
            showToast(getActivity(), getString(R.string.no_internet));
        }

        initCustomSpinner_branch();

        btnContinue = (Button) view.findViewById(R.id.btn_continue);
        accountName = (EditText) view.findViewById(R.id.txt_accountholdername);
        accountNo = (EditText) view.findViewById(R.id.txt_accountno);
        shiftCode = (EditText) view.findViewById(R.id.txt_swift_code);


    /*    accountNo.setFilters(new InputFilter[]{EMOJI_FILTER});
        accountName.setFilters(new InputFilter[]{EMOJI_FILTER});
        shiftCode.setFilters(new InputFilter[]{EMOJI_FILTER});*/


        fragmentManager = getActivity().getSupportFragmentManager();
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnline(getActivity())) {
                    if (isValidateUi()) {
                        /**
                         * isUpdate is true then update bank information that means editing or changing the agent bank details of already existing information.
                         * isupdate--false---adding bank information
                         */
                        if (isUpdate) {
                            accountName.setError(null);
                            accountNo.setError(null);
                            shiftCode.setError(null);
                            updateBankInfo();
                        } else {
                            postBankInfo();
                        }

                        hideKeyboard(context);
                    }
                } else {
                    showToast(getActivity(), getString(R.string.no_internet));
                }

            }
        });
        if (!isUpdate) {
            getAgentBankInfo(CommonConstants.AGENT_ID,true);
        }
        return view;
    }


    private JsonObject agentBankDetails(boolean isUpdate) {
        Log.d(TAG, "agentBankDetails: "+id);
        agentBankInfo.setIsActive(true);
        agentBankInfo.setAccountHolderName(straccountname);
        agentBankInfo.setAccountNumber(straccountno);
        agentBankInfo.setBankId("" + branchListResults.get(spinnerCustom_brach.getSelectedItemPosition()).getId());
        agentBankInfo.setAgentId("" + CommonConstants.AGENT_ID);
        /**
         * isUpdate --true-- setid() as what we get in getbankinfo .
         * isUpdate --false--setId(0) means adding bank info
         */
        if (isUpdate) {
            agentBankInfo.setId(id);
        } else {
            agentBankInfo.setId(0);
        }

        return new Gson().toJsonTree(agentBankInfo)
                .getAsJsonObject();
    }


    private void postBankInfo() {
        showDialog(getActivity(), getString(R.string.authenticating_alert));
        JsonObject object = agentBankDetails(false);
        Log.d(TAG, "postBankInfo: "+agentBankDetails(false).toString());
        MyServices service = ServiceFactory.createRetrofitService(getActivity(), MyServices.class);
        operatorSubscription = service.postBankInfo(object)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BankInfoResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        //Toast.makeText(getActivity(), getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(BankInfoResponseModel bankInfoResponseModel) {
                        hideDialog();
                        if (bankInfoResponseModel.getIsSuccess()) {
                            showToast(context, bankInfoResponseModel.getEndUserMessage());
                            replaceFragment(getActivity(), MAIN_CONTAINER, new Update_IdProofFragment(), TAG, Update_IdProofFragment.TAG);

                        } else {
                            showToast(context, bankInfoResponseModel.getEndUserMessage());
                        }

                    }
                });

    }

    private void updateBankInfo() {
        showDialog(getActivity(), getString(R.string.authenticating_alert));
        JsonObject object = agentBankDetails(true);
        MyServices service = ServiceFactory.createRetrofitService(getActivity(), MyServices.class);
        operatorSubscription = service.updateBankInfo(object)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BankInfoResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        // Toast.makeText(getActivity(), getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(BankInfoResponseModel bankInfoResponseModel) {
                        hideDialog();
                        if (bankInfoResponseModel.getIsSuccess()) {
                            showToast(context, bankInfoResponseModel.getEndUserMessage());
                            isUpdate = true;
                            btnContinue.setText(getString(R.string.update_btn));
                            //  getRequestBank(CommonConstants.BANK_CATEGORY_ID);
                            getAgentBankInfo(CommonConstants.AGENT_ID,false);


                        } else {
                            showToast(context, bankInfoResponseModel.getEndUserMessage());
                        }

                    }
                });

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        getRequestBank(CommonConstants.BANK_CATEGORY_ID);
    }

    private boolean isValidateUi() {
        boolean status = true;
        straccountname = accountName.getText().toString().trim();
        straccountno = accountNo.getText().toString();
        strshiftcode = shiftCode.getText().toString();


        if (straccountname.isEmpty() ) {
            status = false;
            accountName.setError(getString(R.string.accountHolder_Name_is_required));
            accountName.requestFocusFromTouch();
        }else if (accountName.getText().length() < 3) {
            status = false;
            accountName.setError(getString(R.string.Name_is_required));
            accountName.requestFocusFromTouch();
        } else if (straccountno.isEmpty()||accountNo.getText().length()<1) {
            status = false;
            accountNo.setError(getString(R.string.accountNumber_is_required));
            accountNo.requestFocusFromTouch();
        } else if (bankListResults.isEmpty()) {
            status = false;
            Toast.makeText(context, getString(R.string.bank_is_required), Toast.LENGTH_SHORT).show();
        } else if (branchListResults.isEmpty()) {
            status = false;
            Toast.makeText(context, getString(R.string.branch_is_required), Toast.LENGTH_SHORT).show();
        } else if (strshiftcode.isEmpty()) {
            status = false;
            shiftCode.setError(getString(R.string.shiftcode_is_required));
            /*shiftCode.requestFocusFromTouch();*/
        }
        return status;
    }

    private void getRequestBranch(String providerType) {
        showDialog(getActivity(), getString(R.string.authenticating_alert));
        String data = BuildConfig.LOCAL_URL + ApiConstants.BRANCH_REQUESTS + providerType;
        MyServices service = ServiceFactory.createRetrofitService(context, MyServices.class);
        operatorSubscription = service.getBranchRequest(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Branch>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        //Toast.makeText(context, getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(Branch branch) {
                        hideDialog();
                        branchListResults = (ArrayList<Branch.ListResult>) branch.getListResult();

                        branchArrayList = new ArrayList();
                        if (branch.getListResult().size() > 0) {
                            for (int i = 0; i < branch.getListResult().size(); i++) {
                                branchArrayList.add(branch.getListResult().get(i).getBranchName());
                            }
                        } else {
                            branchArrayList.clear();
                            branchArrayAdapter.notifyDataSetChanged();
                            shiftCode.setText("");
                        }
                        branchArrayAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, branchArrayList);
                        branchArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner
                        spinnerCustom_brach.setAdapter(branchArrayAdapter);
                        /*customSpinnerAdapter = new Update_BankDetailFragment.CustomSpinnerAdapter(getActivity(), branchArrayList, false);
                        spinnerCustom_brach.setAdapter(customSpinnerAdapter);*/
/**
 *isUpdate is false ,when getAgentBankInfo is empty and also Bydefault it is false
 */


                    }

                });
    }

    private void getRequestBank(String providerType) {
        MyServices service = ServiceFactory.createRetrofitService(context, MyServices.class);
        operatorSubscription = service.getBusinessRequest(ApiConstants.BUSINESS_CAT_REQUESTS + providerType)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BusinessCategoryModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        //Toast.makeText(context, getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(BusinessCategoryModel businessCategoryModel) {
                        hideDialog();
                        bankListResults = (ArrayList<BusinessCategoryModel.ListResult>) businessCategoryModel.getListResult();
/**
 *isUpdate is false ,when getAgentBankInfo is empty and also Bydefault it is false
 */
                        if (!isUpdate)
                            for (int i = 0; i < businessCategoryModel.getListResult().size(); i++) {
                                bankArrayList.add(businessCategoryModel.getListResult().get(i).getDescription());

                            }
                        bankArrayAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, bankArrayList);
                        bankArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner

                        spinnerCustom_bank.setAdapter(bankArrayAdapter);
                       /* Update_BankDetailFragment.CustomSpinnerAdapterBank customSpinnerAdapterBank = new Update_BankDetailFragment.CustomSpinnerAdapterBank(getActivity(), bankArrayList);
                        spinnerCustom_bank.setAdapter(customSpinnerAdapterBank);*/
                    }

                });


    }

    private void getAgentBankInfo(String agentId, final boolean isDirect) {
        MyServices service = ServiceFactory.createRetrofitService(context, MyServices.class);
        operatorSubscription = service.GetAgentBankInfo(ApiConstants.GET_AGENT_BANK_INFO + agentId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GetBankInfoModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            hideDialog();
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        // Toast.makeText(context, getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(GetBankInfoModel getBankInfoModel) {
                        hideDialog();
                        Log.d(TAG, "getbank"+getBankInfoModel.getListResult().get(0).getId());
                        if (!getBankInfoModel.getListResult().isEmpty()) {
                            isUpdate = true;
                            accountName.setText(getBankInfoModel.getListResult().get(0).getAccountHolderName());
                            accountNo.setText(getBankInfoModel.getListResult().get(0).getAccountNumber());
                            id = getBankInfoModel.getListResult().get(0).getId();
                            spinnerCustom_bank.setSelection(bankArrayList.indexOf(getBankInfoModel.getListResult().get(0).getBankName()));
                            spinnerCustom_brach.setSelection(branchArrayList.indexOf(getBankInfoModel.getListResult().get(0).getBranchName()));
                            strCreatedby = getBankInfoModel.getListResult().get(0).getCreatedBy();
                            btnContinue.setText(getString(R.string.update_btn));
                        } else {
                            isUpdate = false;
                            btnContinue.setText(getString(R.string.continue_btn));
                        }

                        if(!isDirect){
                            replaceFragment(getActivity(), MAIN_CONTAINER, new Update_IdProofFragment(), TAG, Update_IdProofFragment.TAG);
                        }

                    }

                });
    }

    private void initCustomSpinner_branch() {
        spinnerCustom_brach = (Spinner) view.findViewById(R.id.spinner_branch);
        spinnerCustom_brach.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event != null && event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    InputMethodManager imm = ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE));
                    boolean isKeyboardUp = imm.isAcceptingText();

                    if (isKeyboardUp)
                    {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
                return false;
            }
        }) ;
        spinnerCustom_brach.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {

                //String item = parent.getItemAtPosition(position).toString();
                if (!branchListResults.isEmpty()) {
                    shiftCode.setText(branchListResults.get(i).getSwiftCode());
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    private void initCustomSpinner_bank() {

        spinnerCustom_bank = (Spinner) view.findViewById(R.id.spinner_bank);
        //  spinnerCustom_bank.setPrompt("testing");
        spinnerCustom_bank.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event != null && event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    InputMethodManager imm = ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE));
                    boolean isKeyboardUp = imm.isAcceptingText();

                    if (isKeyboardUp)
                    {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }

                }
                return false;
            }
        }) ;
        spinnerCustom_bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                // spinnerCustom_brach.performClick();

               // bankId = (int) parent.getSelectedItemId();
                //bankId = bankListResults.get((int) parent.getSelectedItemId()).getId();

                if (isOnline(getActivity())) {
                    if (!bankListResults.isEmpty() ) {
                        bankId = bankListResults.get(i).getId();
                        getRequestBranch(String.valueOf(bankId));

                    }


                }else{
                    showToast(getActivity(), getString(R.string.no_internet));
                }


            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_personal:

                replaceFragment(getActivity(), MAIN_CONTAINER, new Update_RegistrationViewFragment(), TAG, Update_RegistrationViewFragment.TAG);
                //showToast(getActivity(), getString(R.string.please_Fill_The_Bank_Details));
                break;
            case R.id.btn_id:
                accountName.setError(null);
                accountNo.setError(null);
                shiftCode.setError(null);
                if(getActivity().getCurrentFocus() != null)
                {
                    InputMethodManager inputManager =
                            (InputMethodManager) getActivity().
                                    getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(
                            getActivity().getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }
                if (isOnline(getActivity())) {
                    if (isValidateUi()) {
                        /**
                         * isUpdate is true then update bank information that means editing or changing the agent bank details of already existing information.
                         * isupdate--false---adding bank information
                         */
                        if (isUpdate) {
                            updateBankInfo();
                        } else {
                            postBankInfo();
                        }

                        hideKeyboard(context);
                    }
                } else {
                    showToast(getActivity(), getString(R.string.no_internet));
                }


                break;
            case R.id.btn_cancel:
                replaceFragment(getActivity(), MAIN_CONTAINER, new MainFragment(), TAG, MainFragment.TAG);
                break;

        }
    }


    public void ReplcaFragment(android.support.v4.app.Fragment fragment) {
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }



}
