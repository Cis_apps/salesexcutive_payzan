package calibrage.payzanagent.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import calibrage.payzanagent.R;
import calibrage.payzanagent.controls.CommonTextView;
import calibrage.payzanagent.model.AddAgent;
import calibrage.payzanagent.model.AgentPersonalInfo;
import calibrage.payzanagent.model.AgentRequestModel;
import calibrage.payzanagent.model.BusinessCategoryModel;
import calibrage.payzanagent.model.DistrictModel;
import calibrage.payzanagent.model.GetPersonalInfoModel;
import calibrage.payzanagent.model.InProgressResponseModel;
import calibrage.payzanagent.model.MandalModel;
import calibrage.payzanagent.model.PersonalInfoResponseModel;
import calibrage.payzanagent.model.PostUpdatePersonalInfo;
import calibrage.payzanagent.model.ProvinceModel;
import calibrage.payzanagent.model.StatesModel;
import calibrage.payzanagent.model.VillageModel;
import calibrage.payzanagent.networkservice.ApiConstants;
import calibrage.payzanagent.networkservice.MyServices;
import calibrage.payzanagent.networkservice.ServiceFactory;
import calibrage.payzanagent.service.GPSTracker;
import calibrage.payzanagent.utils.CommonConstants;
import calibrage.payzanagent.utils.NCBTextInputLayout;
import calibrage.payzanagent.utils.SharedPrefsData;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static calibrage.payzanagent.utils.CommonUtil.MY_PERMISSIONS_REQUEST_LOCATION;


public class Update_RegistrationViewFragment extends BaseFragment implements OnMapReadyCallback, View.OnClickListener {

    public static final String TAG = Update_RegistrationViewFragment.class.getSimpleName();

    private Button btnContinue;
    View view;
    FragmentManager fragmentManager;
    EditText edtFirstName, edtUserName, edtPassWord, edtMobile, edtEmail, edtAddress1, edtAddress2, edtLandMark, edtPincode, edtMiddleName, edtLastName, edtDOB;
    private InProgressResponseModel agentRequestModel;
    private StatesModel statesModel;
    private Context context;
    Spinner spinnerBusinessCat, spinnerState, spinnerTitleType, spinnerProivne, spinnerDistrict, spinnerMandal, spinnerVillage, spinnerGender;
    private Subscription operatorSubscription;
    public static Toolbar toolbar;
    private ArrayList<AgentRequestModel.ListResult> listResults;
    private ArrayList<StatesModel.ListResult> listResultArrayList;
    private ArrayList<AgentRequestModel.ListResult> titleResultArrayList;
    MapView mMapView;
    private GoogleMap googleMap;
    private CommonTextView latlog;
    private GPSTracker gpsTracker;
    private Button personalButton, bankButton, idButton, documentButton, btnCancel;
    //If isDirectAgent is true  then it will be AgentRegistration is Direct New Agent .
    private boolean isDirectAgent;
    private Double lat = 0.0, log = 0.0;
    LocationManager locationManager;
    String provider, aspNetId, strCurrentDate;
    int provinceId, districtId, mandalId, villageId;
    public AddAgent addAgent;
    LinearLayout linearLayout;
    private NCBTextInputLayout agentNameTXT;
    private NCBTextInputLayout il_passqword;
    Location lastLocation;
    int intAgentRequestId;
    private int mYear, mMonth, mDay;
    //It is the key value to recogize whether update  personal  info or add  the info
    private boolean is_exisisting_user = false;
    // TextView provincetxt;

    private String strfirstname, strusername, strpass, strmobile, stremail, straddress1, straddress2, strlandmark, strpin, currentDatetime, strlastname, strmiddlename = " ", strbundleMobile, strCreatedBy;
    private int agentRequestId;
    private AgentPersonalInfo agentPersonalInfo;

    ArrayList<String> businessArrayList = new ArrayList<String>();
    private ArrayList<BusinessCategoryModel.ListResult> businessListResults = new ArrayList<>();

    ArrayList<String> titleArrayList = new ArrayList<String>();
    private ArrayList<BusinessCategoryModel.ListResult> titleListResults = new ArrayList<>();

    ArrayList<String> genderArrayList = new ArrayList<String>();
    private ArrayList<BusinessCategoryModel.ListResult> genderListResults = new ArrayList<>();

    ArrayList<String> statesArrayList = new ArrayList<String>();
    private ArrayList<StatesModel.ListResult> stateListResults = new ArrayList<>();

    ArrayList<String> provinceArrayList = new ArrayList<String>();
    private ArrayList<ProvinceModel.ListResult> provinceListResults = new ArrayList<>();

    ArrayList<String> districtArrayList = new ArrayList<String>();
    private ArrayList<DistrictModel.ListResult> districtListResults = new ArrayList<>();

    ArrayList<String> mandalArrayList = new ArrayList<String>();
    private ArrayList<MandalModel.ListResult> mandalListResults = new ArrayList<>();

    ArrayList<String> villageArrayList = new ArrayList<String>();
    private ArrayList<VillageModel.ListResult> villageListResults = new ArrayList<>();

    ArrayList<String> updateArrayList = new ArrayList<String>();
    // private ArrayList<GetPersonalInfoModel.RE> updateListResults = new ArrayList<>();
    private GetPersonalInfoModel.Result updateListResults;

    Update_RegistrationViewFragment.CustomSpinnerAdapter customSpinnerAdapter;
    private Subscription mRegisterSubscription;

    ArrayAdapter titleArrayAdapter;
    //String id;
    private String parentAspNetId;
    private String createdBy;
    private int id;
    ScrollView scrollview;
    private String dateOfBirth;
    private boolean isFirstTime = false;

    public Update_RegistrationViewFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_registration_view_update,container, false);
        context = this.getActivity();
        view.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        if(getActivity().getCurrentFocus() != null)
        {
            InputMethodManager inputManager =
                    (InputMethodManager) getActivity().
                            getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(
                    getActivity().getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
        initMap(savedInstanceState);
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        provider = locationManager.getBestProvider(new Criteria(), false);
        checkLocationPermission(context);
        initBusinessCatSpinner();
        initTitleSpinner();
        initGenderSpinner();
        initProvinceSpinner();
        initDistrictSpinner();
        initMandalSpinner();
        initVillageSpinner();
        if (isOnline(getActivity())) {
            getRequest(CommonConstants.BUSINESS_CATEGORY_ID);
            getRequestTitle(CommonConstants.TITLE_ID);
            getRequestProvince(CommonConstants.PROVINCE_NAME);
            getGenderType(CommonConstants.GENDER_ID);

        } else {
            showToast(getActivity(), getString(R.string.no_internet));
        }

        listResults = new ArrayList();
        listResultArrayList = new ArrayList();
        updateArrayList = new ArrayList();
        titleResultArrayList = new ArrayList();
        provinceArrayList = new ArrayList();
        addAgent = new AddAgent();
        agentPersonalInfo = new AgentPersonalInfo();
        currentDatetime = SharedPrefsData.getInstance(context).getStringFromSharedPrefs("datetime");
        btnContinue = (Button) view.findViewById(R.id.btn_continue);
        edtFirstName = (EditText) view.findViewById(R.id.edt_firstName);
        edtMiddleName = (EditText) view.findViewById(R.id.edt_middleName);
        edtLastName = (EditText) view.findViewById(R.id.edt_lastName);
        edtUserName = (EditText) view.findViewById(R.id.edt_userName);
        edtPassWord = (EditText) view.findViewById(R.id.edt_password);
        edtMobile = (EditText) view.findViewById(R.id.edt_mobileno);
        edtEmail = (EditText) view.findViewById(R.id.edt_email);
        edtAddress1 = (EditText) view.findViewById(R.id.edt_address1);
        edtAddress2 = (EditText) view.findViewById(R.id.edt_address2);
        edtLandMark = (EditText) view.findViewById(R.id.edt_land_mark);
        edtPincode = (EditText) view.findViewById(R.id.edt_pincode);
        edtDOB = (EditText) view.findViewById(R.id.edt_DOB);
        btnCancel = (Button) view.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);
        edtMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if (charSequence.length() > 0) {
                    // .setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                String x = "";
                x=s.toString();
                if(x.startsWith("0")){
                    edtMobile.setText("");
                }
            }
        });
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if (charSequence.length() > 0) {

                    // .setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                String x = "";
                x=s.toString();
                if(x.startsWith(".")){
                    edtEmail.setText("");
                }
            }
        });
      /*  edtFirstName.setFilters(new InputFilter[]{EMOJI_FILTER});
        edtMiddleName.setFilters(new InputFilter[]{EMOJI_FILTER});
        edtLastName.setFilters(new InputFilter[]{EMOJI_FILTER});
        edtPassWord.setFilters(new InputFilter[]{EMOJI_FILTER});
        edtMobile.setFilters(new InputFilter[]{EMOJI_FILTER});
        edtEmail.setFilters(new InputFilter[]{EMOJI_FILTER});
        edtAddress1.setFilters(new InputFilter[]{EMOJI_FILTER});
        edtAddress2.setFilters(new InputFilter[]{EMOJI_FILTER});
        edtLandMark.setFilters(new InputFilter[]{EMOJI_FILTER});
        edtPincode.setFilters(new InputFilter[]{EMOJI_FILTER});*/




        scrollview = view.findViewById(R.id.scrollview);
        scrollview.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if (event != null && event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    InputMethodManager imm = ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE));
                    boolean isKeyboardUp = imm.isAcceptingText();

                    if (isKeyboardUp)
                    {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
                return false;
            }
        });
        /*provincetxt = view.findViewById(R.id.txt_province);
        provincetxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerProivne.setVisibility(View.VISIBLE);
                spinnerDistrict.setVisibility(View.VISIBLE);
                spinnerMandal.setVisibility(View.VISIBLE);
                spinnerVillage.setVisibility(View.VISIBLE);
                edtPincode.setVisibility(View.VISIBLE);

            }
        });*/

        personalButton = (Button) view.findViewById(R.id.btn_personal);
        // edtFirstName.setKeyListener(DigitsKeyListener.getInstance("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"));
        bankButton = (Button) view.findViewById(R.id.btn_bank);
        idButton = (Button) view.findViewById(R.id.btn_id);
        il_passqword = view.findViewById(R.id.il_passqword);
        documentButton = (Button) view.findViewById(R.id.btn_doc);
        il_passqword.setVisibility(View.VISIBLE);
        edtDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDatePicker();

            }
        });
        personalButton.setOnClickListener(this);
        bankButton.setOnClickListener(this);
        latlog = (CommonTextView) view.findViewById(R.id.latlog);
        latlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gpsTracker = new GPSTracker(context);
                if (gpsTracker.canGetLocation()) {
                    lat = gpsTracker.getLatitude();
                    log = gpsTracker.getLongitude();
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    lastLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (lat == 0.0 && log == 0.0 && lastLocation != null) {
                        lat = lastLocation.getLatitude();
                        log = lastLocation.getLongitude();
                    }
                } else {
                    gpsTracker.showSettingsAlert();
                }
                LatLng sydney = new LatLng(lat, log);
                googleMap.addMarker(new MarkerOptions().position(sydney).title("Marker Title").snippet("Marker Description"));

            }
        });


        fragmentManager = getActivity().getSupportFragmentManager();
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(context);
              /*  InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);*/
                if (isOnline(getActivity())) {
                    if (isValidateUi()) {

                        if(btnContinue.getText().toString().equalsIgnoreCase("continue")){
                            updatePersonalInfoNew();
                        }
                        if(is_exisisting_user){
                            updatePersonalInfo();
                        }else {
                            postPersonalInfo();
                        }


                    }
                } else {
                    showToast(getActivity(), getString(R.string.no_internet));
                }

            }
        });

        Bundle bundle = getArguments();
        if (bundle != null) {
            isDirectAgent = false;
//            agentRequestModel = bundle.getParcelable("request");
            int pos = bundle.getInt("position");
            intAgentRequestId = Integer.parseInt(bundle.getString("intAgentRequestId"));
            String name;
            if (bundle.getString("edtMiddleName") != null) {
                edtMiddleName.setText(bundle.getString("edtMiddleName"));
            } else {
                edtMiddleName.setText("");
            }
            edtFirstName.setText(bundle.getString("edtFirstName"));
            edtLastName.setText(bundle.getString("edtLastName"));
            edtMobile.setText(bundle.getString("edtMobile"));
            // Set the EditText input type null
           /* edtMobile.setInputType(InputType.TYPE_NULL);*/
            edtMobile.setFocusable(false);
            edtMobile.setFocusableInTouchMode(false);
            edtMobile.setClickable(false);
            strbundleMobile = bundle.getString("strbundleMobile");
            edtEmail.setText(bundle.getString("edtEmail"));
            edtAddress1.setText(bundle.getString("edtAddress1"));
            edtAddress2.setText(bundle.getString("edtAddress2"));
            edtLandMark.setText(bundle.getString("edtLandmark"));
        } else {
            isDirectAgent = true;
        }
        if (CommonConstants.Is_New_Agent_Request) {

        } else {
            if (!isFirstTime) {
                getRequestPersonalInfo(CommonConstants.MOBILE_NUMBER);
                isFirstTime = true;
            }

        }

        return view;
    }


    private void hideSoftKeyboard(RegistrationViewFragment registrationViewFragment) {
        view.clearFocus();
        InputMethodManager imm = (InputMethodManager) getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void updatePersonalInfo() {
        showDialog(getActivity(), getString(R.string.authenticating_alert));
        JsonObject object = updatePersonalInfoObject();
        MyServices service = ServiceFactory.createRetrofitService(getActivity(), MyServices.class);
        mRegisterSubscription = service.updatePersonalInfo(object)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PersonalInfoResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        //Toast.makeText(getActivity(), getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(PersonalInfoResponseModel personalInfoResponseModel) {
                        hideDialog();
                        if (personalInfoResponseModel.getIsSuccess()) {
                            showToast(context, "" + personalInfoResponseModel.getEndUserMessage());

                            CommonConstants.AGENT_ID = personalInfoResponseModel.getResult().getAspNetUserId();
                            replaceFragment(getActivity(), MAIN_CONTAINER, new Update_BankDetailFragment(), TAG, Update_BankDetailFragment.TAG);

                        } else {
                            showToast(context, "" + personalInfoResponseModel.getEndUserMessage());
                        }

                    }

                });

    }
    private void updatePersonalInfoNew() {
        showDialog(getActivity(), getString(R.string.authenticating_alert));
        JsonObject object = updatePersonalInfoObject();
        MyServices service = ServiceFactory.createRetrofitService(getActivity(), MyServices.class);
        mRegisterSubscription = service.updatePersonalInfo(object)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PersonalInfoResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        //Toast.makeText(getActivity(), getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(PersonalInfoResponseModel personalInfoResponseModel) {
                        hideDialog();
                        if (personalInfoResponseModel.getIsSuccess()) {
                            showToast(context, "" + personalInfoResponseModel.getEndUserMessage());

                            CommonConstants.AGENT_ID = personalInfoResponseModel.getResult().getAspNetUserId();
                            replaceFragment(getActivity(), MAIN_CONTAINER, new NewAgent_BankDetailFragment(), TAG, NewAgent_BankDetailFragment.TAG);

                        } /*else {
                            showToast(context, "" + personalInfoResponseModel.getEndUserMessage());
                        }*/

                    }

                });

    }


    private void getRequestPersonalInfo(String providerType) {
        showDialog(getActivity(), getString(R.string.authenticating_alert));
        MyServices service = ServiceFactory.createRetrofitService(context, MyServices.class);
        operatorSubscription = service.getUpdatePersonalInfoRequest(ApiConstants.PERSONAL_INFO_REQUESTS + providerType)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GetPersonalInfoModel>() {
                    @Override
                    public void onCompleted() {
                        //  Toast.makeText(context, "check", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                      //  Toast.makeText(context, getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(GetPersonalInfoModel getPersonalInfoModel) {
                        hideDialog();
                        Log.d("responsesefddgddfvdfb", getPersonalInfoModel.getResult().toString());
                        updateListResults = getPersonalInfoModel.getResult();
                        if (updateListResults != null) {
                            is_exisisting_user = true;
                            spinnerTitleType.setSelection(titleArrayList.indexOf(getPersonalInfoModel.getResult().getTitleTypeName()));
                            edtFirstName.setText(getPersonalInfoModel.getResult().getFirstName());
                            if (getPersonalInfoModel.getResult().getMiddleName() == null) {
                                edtMiddleName.setText(" ");
                            } else {
                                edtMiddleName.setText(getPersonalInfoModel.getResult().getMiddleName());
                            }
                            edtLastName.setText(getPersonalInfoModel.getResult().getLastName());
                            spinnerBusinessCat.setSelection(businessArrayList.indexOf(getPersonalInfoModel.getResult().getBusinessCategoryName()));
                            edtMobile.setText(getPersonalInfoModel.getResult().getPhone());
                            il_passqword.setVisibility(View.GONE);
                            spinnerGender.setSelection(genderArrayList.indexOf(getPersonalInfoModel.getResult().getGenderType()));
                            edtEmail.setText(getPersonalInfoModel.getResult().getEmail());
                            dateOfBirth = getPersonalInfoModel.getResult().getDOB();
                            edtDOB.setText(formatDateTimeUi());
                            edtAddress1.setText(getPersonalInfoModel.getResult().getAddress1());
                            edtAddress2.setText(getPersonalInfoModel.getResult().getAddress2());
                            edtLandMark.setText(getPersonalInfoModel.getResult().getLandmark());
                            spinnerProivne.setSelection(provinceArrayList.indexOf(getPersonalInfoModel.getResult().getProvinceName()));
                            edtPincode.setText("" + getPersonalInfoModel.getResult().getPostCode());
                            strCreatedBy = getPersonalInfoModel.getResult().getCreatedBy();
                            agentRequestId = getPersonalInfoModel.getResult().getAgentRequestId();
                            if (getPersonalInfoModel.getResult().getAspNetUserId() != null) {
                                aspNetId = getPersonalInfoModel.getResult().getAspNetUserId();
                            }
                            createdBy = getPersonalInfoModel.getResult().getCreated().toString();
                            CommonConstants.AGENT_ID = getPersonalInfoModel.getResult().getAspNetUserId();
                            id = getPersonalInfoModel.getResult().getId();
                            btnContinue.setText(getString(R.string.update_btn));
                        } else {
                            is_exisisting_user = false;
                            btnContinue.setText(getString(R.string.continue_btn));
                        }


                    }

                });
    }

    private void initGenderSpinner() {

        spinnerGender = (Spinner) view.findViewById(R.id.spinner_gender_cat);
        spinnerGender.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event != null && event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    InputMethodManager imm = ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE));
                    boolean isKeyboardUp = imm.isAcceptingText();

                    if (isKeyboardUp)
                    {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
                return false;
            }
        }) ;
        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initVillageSpinner() {
        spinnerVillage = (Spinner) view.findViewById(R.id.spinner_village);
        spinnerVillage.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event != null && event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    InputMethodManager imm = ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE));
                    boolean isKeyboardUp = imm.isAcceptingText();

                    if (isKeyboardUp)
                    {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
                return false;
            }
        }) ;
        spinnerVillage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getItemAtPosition(position).toString();
                if (!villageListResults.isEmpty()) {
                    edtPincode.setText(String.valueOf(villageListResults.get(position).getPostCode()));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initMandalSpinner() {
        spinnerMandal = (Spinner) view.findViewById(R.id.spinner_mandal);
        spinnerMandal.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event != null && event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    InputMethodManager imm = ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE));
                    boolean isKeyboardUp = imm.isAcceptingText();

                    if (isKeyboardUp)
                    {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
                return false;
            }
        }) ;
        spinnerMandal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getItemAtPosition(position).toString();
                if (!mandalListResults.isEmpty()) {
                    mandalId = mandalListResults.get((int) parent.getSelectedItemId()).getId();
                    if (isOnline(getActivity())) {
                        getRequestVillage(String.valueOf(mandalId));
                    } else {
                        showToast(getActivity(), getString(R.string.no_internet));
                    }


                } else {
                    Toast.makeText(parent.getContext(), getString(R.string.input_Not_Valid), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getRequestVillage(String providerType) {
        MyServices service = ServiceFactory.createRetrofitService(context, MyServices.class);
        operatorSubscription = service.getVillageRequest(ApiConstants.VILLAGE_REQUESTS + providerType)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<VillageModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        Toast.makeText(context, getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(VillageModel villageModel) {
                        hideDialog();
                        Log.d("response", villageModel.getIsSuccess().toString());
                        villageListResults = (ArrayList<VillageModel.ListResult>) villageModel.getListResult();
                        villageArrayList = new ArrayList();
                        if (villageModel.getListResult().size() > 0) {

                            for (int i = 0; i < villageModel.getListResult().size(); i++) {
                                villageArrayList.add(villageModel.getListResult().get(i).getName());
                            }
                        } else {
                            villageArrayList.clear();
                            edtPincode.setText(" ");
                            customSpinnerAdapter.notifyDataSetChanged();
                        }

                        customSpinnerAdapter = new Update_RegistrationViewFragment.CustomSpinnerAdapter(getActivity(), villageArrayList);
                        spinnerVillage.setAdapter(customSpinnerAdapter);
                       /* if (CommonConstants.Is_New_Agent_Request) {

                        } else {
                            if (!isFirstTime) {
                                getRequestPersonalInfo(strbundleMobile);
                                isFirstTime = true;
                            }

                        }*/
                        if (is_exisisting_user)
                            spinnerVillage.setSelection(villageArrayList.indexOf(updateListResults.getVillageName()));

                    }

                });
    }

    private void openDatePicker() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        edtDOB.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        formatDateTime();

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();

    }

    private void initDistrictSpinner() {
        spinnerDistrict = (Spinner) view.findViewById(R.id.spinner_district);
        spinnerDistrict.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event != null && event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    InputMethodManager imm = ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE));
                    boolean isKeyboardUp = imm.isAcceptingText();

                    if (isKeyboardUp)
                    {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
                return false;
            }
        }) ;
        spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getItemAtPosition(position).toString();
                if (!districtListResults.isEmpty()) {
                    districtId = districtListResults.get((int) parent.getSelectedItemId()).getId();
                    if (isOnline(getActivity())) {
                        getRequestMandal(String.valueOf(districtId));
                    } else {
                        showToast(getActivity(), getString(R.string.no_internet));
                    }

                } else {
                    Toast.makeText(parent.getContext(), getString(R.string.input_Not_Valid), Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getRequestMandal(String providerType) {
        MyServices service = ServiceFactory.createRetrofitService(context, MyServices.class);
        operatorSubscription = service.getMandalRequest(ApiConstants.MANDAL_REQUESTS + providerType)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MandalModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        Toast.makeText(context, getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(MandalModel mandalModel) {
                        hideDialog();
                        Log.d("response", mandalModel.getIsSuccess().toString());
                        mandalListResults = (ArrayList<MandalModel.ListResult>) mandalModel.getListResult();

                        mandalArrayList = new ArrayList();
                        if (mandalModel.getListResult().size() > 0) {

                            for (int i = 0; i < mandalModel.getListResult().size(); i++) {
                                mandalArrayList.add(mandalModel.getListResult().get(i).getName());

                            }
                        } else {
                            mandalArrayList.clear();
                            villageArrayList.clear();
                            edtPincode.setText(" ");
                            customSpinnerAdapter.notifyDataSetChanged();
                        }


                        customSpinnerAdapter = new Update_RegistrationViewFragment.CustomSpinnerAdapter(getActivity(), mandalArrayList);
                        spinnerMandal.setAdapter(customSpinnerAdapter);

                        if (is_exisisting_user)
                            spinnerMandal.setSelection(mandalArrayList.indexOf(updateListResults.getMandalName()));
                    }

                });
    }


    private void getRequestProvince(String providerType) {
        MyServices service = ServiceFactory.createRetrofitService(context, MyServices.class);
        operatorSubscription = service.getProvinceRequest(ApiConstants.PROVINCE_REQUESTS + providerType)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProvinceModel>() {
                    @Override
                    public void onCompleted() {
                        //   Toast.makeText(context, "check", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        Toast.makeText(context, getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(ProvinceModel provinceModel) {
                        hideDialog();
                        Log.d("response", provinceModel.getIsSuccess().toString());
                        /*provinceArrayList.add(0, "--Select Province--");*/
                        provinceListResults = (ArrayList<ProvinceModel.ListResult>) provinceModel.getListResult();
                        for (int i = 0; i < provinceModel.getListResult().size(); i++) {
                            provinceArrayList.add(provinceModel.getListResult().get(i).getName());


                        }


                        customSpinnerAdapter = new Update_RegistrationViewFragment.CustomSpinnerAdapter(getActivity(), provinceArrayList);
                        spinnerProivne.setAdapter(customSpinnerAdapter);
                    }

                });

    }

    private void initProvinceSpinner() {

        spinnerProivne = (Spinner) view.findViewById(R.id.spinner_province);
        spinnerProivne.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event != null && event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    InputMethodManager imm = ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE));
                    boolean isKeyboardUp = imm.isAcceptingText();

                    if (isKeyboardUp)
                    {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
                return false;
            }
        }) ;
        spinnerProivne.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getItemAtPosition(position).toString();
                if (!provinceListResults.isEmpty()) {
                    provinceId = provinceListResults.get((int) (parent.getSelectedItemId())).getId();
                    if (isOnline(getActivity())) {
                        getRequestDistrict(String.valueOf(provinceId));
                    } else {
                        showToast(getActivity(), getString(R.string.no_internet));
                    }

                } else {
                    Toast.makeText(parent.getContext(), getString(R.string.input_Not_Valid), Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void getRequestDistrict(String providerType) {
        MyServices service = ServiceFactory.createRetrofitService(context, MyServices.class);
        operatorSubscription = service.getDistrictRequest(ApiConstants.DISTRICT_REQUESTS + providerType)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<DistrictModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        Toast.makeText(context, getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(DistrictModel districtModel) {
                        hideDialog();
                        districtListResults = (ArrayList<DistrictModel.ListResult>) districtModel.getListResult();
                        districtArrayList = new ArrayList();
                        if (districtModel.getListResult().size() > 0) {
                            for (int i = 0; i < districtModel.getListResult().size(); i++) {
                                districtArrayList.add(districtModel.getListResult().get(i).getName());

                            }
                        } else {
                            districtArrayList.clear();
                            mandalArrayList.clear();
                            villageArrayList.clear();
                            edtPincode.setText(" ");
                            customSpinnerAdapter.notifyDataSetChanged();
                        }


                        customSpinnerAdapter = new Update_RegistrationViewFragment.CustomSpinnerAdapter(getActivity(), districtArrayList);
                        spinnerDistrict.setAdapter(customSpinnerAdapter);
                        if (is_exisisting_user)
                            spinnerDistrict.setSelection(districtArrayList.indexOf(updateListResults.getDistrictName()));
                    }

                });
    }

    private void getRequestTitle(String titleType) {
        showDialog(getActivity(), getString(R.string.authenticating_alert));
        MyServices service = ServiceFactory.createRetrofitService(context, MyServices.class);
        operatorSubscription = service.getBusinessRequest(ApiConstants.BUSINESS_CAT_REQUESTS + Integer.parseInt(titleType))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BusinessCategoryModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        Toast.makeText(context, getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(BusinessCategoryModel businessCategoryModel) {
                        hideDialog();
                        titleListResults = (ArrayList<BusinessCategoryModel.ListResult>) businessCategoryModel.getListResult();
                        titleArrayList.add(0, getString(R.string.select_Title));
                        for (int i = 0; i < businessCategoryModel.getListResult().size(); i++) {
                            titleArrayList.add(businessCategoryModel.getListResult().get(i).getDescription());
                        }
                        titleArrayAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, titleArrayList);
                        titleArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner

                        spinnerTitleType.setAdapter(titleArrayAdapter);
                       /* Update_RegistrationViewFragment.CustomSpinnerAdapter customSpinnerAdapter = new Update_RegistrationViewFragment.CustomSpinnerAdapter(getActivity(), titleArrayList);
                        titleArrayList.add(0, getString(R.string.select_Title));
                        spinnerTitleType.setAdapter(customSpinnerAdapter);*/
                    }

                });
    }

    private void getGenderType(String titleType) {
        showDialog(getActivity(), getString(R.string.authenticating_alert));
        MyServices service = ServiceFactory.createRetrofitService(context, MyServices.class);
        operatorSubscription = service.getBusinessRequest(ApiConstants.BUSINESS_CAT_REQUESTS + Integer.parseInt(titleType))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BusinessCategoryModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        Toast.makeText(context, getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(BusinessCategoryModel businessCategoryModel) {
                        hideDialog();
                        genderListResults = (ArrayList<BusinessCategoryModel.ListResult>) businessCategoryModel.getListResult();
                        genderArrayList.add(0, getString(R.string.select_gender));
                        for (int i = 0; i < businessCategoryModel.getListResult().size(); i++) {
                            genderArrayList.add(businessCategoryModel.getListResult().get(i).getDescription());
                        }
                        Update_RegistrationViewFragment.CustomSpinnerAdapter customSpinnerAdapter = new Update_RegistrationViewFragment.CustomSpinnerAdapter(getActivity(), genderArrayList);
                        spinnerGender.setAdapter(customSpinnerAdapter);


                    }

                });
    }

    private void initTitleSpinner() {
        spinnerTitleType = (Spinner) view.findViewById(R.id.spinner_title_type);
        spinnerTitleType.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event != null && event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    InputMethodManager imm = ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE));
                    boolean isKeyboardUp = imm.isAcceptingText();

                    if (isKeyboardUp)
                    {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
                return false;
            }
        }) ;
        spinnerTitleType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private JsonObject addAgentPersonalInfo() {
        addAgent.setUserName(strusername);
        agentPersonalInfo.setFirstName(strfirstname);
        agentPersonalInfo.setPhone(strmobile);
        agentPersonalInfo.setAgentBusinessCategoryId(businessListResults.get(spinnerBusinessCat.getSelectedItemPosition() - 1).getId());
        agentPersonalInfo.setEmail(stremail);
        agentPersonalInfo.setAddress1(straddress1);
        agentPersonalInfo.setAddress2(straddress2);
        agentPersonalInfo.setPhone(strmobile);
        if (isDirectAgent) {
            agentPersonalInfo.setAgentRequestId(null);
        } else {
            agentPersonalInfo.setAgentRequestId(intAgentRequestId);
        }
        agentPersonalInfo.setLandmark(strlandmark);
        agentPersonalInfo.setParentAspNetUserId("");
        agentPersonalInfo.setPassword(strpass);
        agentPersonalInfo.setTitleTypeId(titleListResults.get(spinnerTitleType.getSelectedItemPosition() - 1).getId());
        agentPersonalInfo.setGenderTypeId(genderListResults.get(spinnerGender.getSelectedItemPosition() - 1).getId());
        agentPersonalInfo.setVillageId(villageListResults.get(spinnerVillage.getSelectedItemPosition()).getId());
        agentPersonalInfo.setParentAspNetUserId(null);
        agentPersonalInfo.setMiddleName(strmiddlename);
        agentPersonalInfo.setLastName(strlastname);
        agentPersonalInfo.setDOB(formatDateTime());
        agentPersonalInfo.setFirstName(strfirstname);
        return new Gson().toJsonTree(agentPersonalInfo)
                .getAsJsonObject();

    }

    private JsonObject updatePersonalInfoObject() {
        PostUpdatePersonalInfo postUpdatePersonalInfo = new PostUpdatePersonalInfo();
        postUpdatePersonalInfo.setFirstName(strfirstname);
        postUpdatePersonalInfo.setPhone(strmobile);
        postUpdatePersonalInfo.setAgentBusinessCategoryId(businessListResults.get(spinnerBusinessCat.getSelectedItemPosition() - 1).getId());
        postUpdatePersonalInfo.setEmail(stremail);
        postUpdatePersonalInfo.setAddress1(straddress1);
        postUpdatePersonalInfo.setAddress2(straddress2);
        postUpdatePersonalInfo.setPhone(strmobile);
        postUpdatePersonalInfo.setAgentRequestId(agentRequestId);
        postUpdatePersonalInfo.setLandmark(strlandmark);
        postUpdatePersonalInfo.setParentAspNetUserId(parentAspNetId);
        postUpdatePersonalInfo.setAspNetUserId(aspNetId);
        postUpdatePersonalInfo.setId(id);
        postUpdatePersonalInfo.setTitleTypeId(titleListResults.get(spinnerTitleType.getSelectedItemPosition() - 1).getId());
        postUpdatePersonalInfo.setGenderTypeId(genderListResults.get(spinnerGender.getSelectedItemPosition() - 1).getId());
        postUpdatePersonalInfo.setVillageId(villageListResults.get(spinnerVillage.getSelectedItemPosition()).getId());
        postUpdatePersonalInfo.setParentAspNetUserId(null);
        postUpdatePersonalInfo.setMiddleName(strmiddlename);
        postUpdatePersonalInfo.setLastName(strlastname);
        postUpdatePersonalInfo.setDOB(currentDatetime);
        postUpdatePersonalInfo.setIsActive(true);
        postUpdatePersonalInfo.setFirstName(strfirstname);
        postUpdatePersonalInfo.setEducationTypeId(null);
        return new Gson().toJsonTree(postUpdatePersonalInfo)
                .getAsJsonObject();

    }



    private void postPersonalInfo() {
        showDialog(getActivity(), getString(R.string.authenticating_alert));
        JsonObject object = addAgentPersonalInfo();
        Log.d(TAG, "postPersonalInfo: "+object.toString());
        MyServices service = ServiceFactory.createRetrofitService(getActivity(), MyServices.class);
        mRegisterSubscription = service.postPersonalInfo(object)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PersonalInfoResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        Toast.makeText(getActivity(), getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(PersonalInfoResponseModel personalInfoResponseModel) {
                        hideDialog();
                        if (personalInfoResponseModel.getIsSuccess()) {
                            Log.d(TAG, "response: "+personalInfoResponseModel.getEndUserMessage());
                            Toast.makeText(getActivity(), personalInfoResponseModel.getEndUserMessage(), Toast.LENGTH_LONG).show();
                            CommonConstants.AGENT_REQUEST_ID = String.valueOf(personalInfoResponseModel.getResult().getAgentRequestId());
                            CommonConstants.AGENT_ID = personalInfoResponseModel.getResult().getAgentId();
                            replaceFragment(getActivity(), MAIN_CONTAINER, new NewAgent_BankDetailFragment(), TAG, NewAgent_BankDetailFragment.TAG);

                        } else {
                            Log.d(TAG, "response: "+personalInfoResponseModel.getEndUserMessage());
                            if(personalInfoResponseModel.getEndUserMessage()==null){
                                Toast.makeText(getActivity(), personalInfoResponseModel.getValidationErrors().get(0).getDescription(), Toast.LENGTH_LONG).show();
                            }else {
                                Toast.makeText(getActivity(), personalInfoResponseModel.getEndUserMessage(), Toast.LENGTH_LONG).show();

                            }

                        }

                    }
                });

    }

    private boolean isValidateUi() {
        boolean status = true;
        strfirstname = edtFirstName.getText().toString().trim();
        strmiddlename = edtMiddleName.getText().toString().trim();
        strlastname = edtLastName.getText().toString().trim();
        //  strusername = edtUserName.getText().toString();
        strpass = edtPassWord.getText().toString();
        strmobile = edtMobile.getText().toString();
        stremail = edtEmail.getText().toString();
        straddress1 = edtAddress1.getText().toString();
        straddress2 = edtAddress2.getText().toString();
        strlandmark = edtLandMark.getText().toString();
        strpin = edtPincode.getText().toString();
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        if (spinnerTitleType.getSelectedItemPosition() == 0) {
            status = false;
            Toast.makeText(context, getString(R.string.select_Title_Type), Toast.LENGTH_SHORT).show();
        } else if (strfirstname.isEmpty()) {
            status = false;
            edtFirstName.setError(getString(R.string.firstName_required));
            edtFirstName.requestFocusFromTouch();
        } else if (edtFirstName.getText().length() < 3) {
            status = false;
            edtFirstName.setError(getString(R.string.valid_firstName_required));
            edtFirstName.requestFocusFromTouch();
        }else if (strlastname.isEmpty()) {
            status = false;
            edtLastName.setError(getString(R.string.lastName_required));
            edtLastName.requestFocusFromTouch();
        }else if (edtLastName.getText().length()<1) {
            status = false;
            edtLastName.setError(getString(R.string.please_enter_lastName));
            edtLastName.requestFocusFromTouch();
        }  else if (spinnerBusinessCat.getSelectedItemPosition() == 0) {
            status = false;
            Toast.makeText(context, getString(R.string.please_select_business_category), Toast.LENGTH_SHORT).show();
        } else if (strmobile.isEmpty() ) {
            status = false;
            edtMobile.setError(getString(R.string.mobileNumber_required));
            edtMobile.requestFocusFromTouch();
        }else if (edtMobile.getText().length() < 10) {
            status = false;
            edtMobile.setError(getString(R.string.valid_mobile));
            edtMobile.requestFocusFromTouch();
        } else if ((strpass.isEmpty() ) && !is_exisisting_user) {
            status = false;
            edtPassWord.setError(getString(R.string.password_required));
            edtPassWord.requestFocusFromTouch();
        } else if ((edtPassWord.getText().length() < 8) && !is_exisisting_user) {
            status = false;
            edtPassWord.setError(getString(R.string.valid_password_required));
            edtPassWord.requestFocusFromTouch();
        }
        else if (spinnerGender.getSelectedItemPosition() == 0) {
            status = false;
            Toast.makeText(context, getString(R.string.select_Gender), Toast.LENGTH_SHORT).show();
        } else if (stremail.isEmpty()) {
            status = false;
            edtEmail.setError(getString(R.string.enter_email));
            edtEmail.requestFocusFromTouch();
        } else if (edtEmail.getText().length()<5 || !emailPattern.matcher(stremail).matches()) {
            status = false;
            edtEmail.setError(getString(R.string.valid_email_required));
            edtEmail.requestFocusFromTouch();
        }
        else if (edtDOB.getText().toString().isEmpty()) {
            status = false;
            Toast.makeText(context, getString(R.string.please_select_date_of_birth), Toast.LENGTH_SHORT).show();
        } else if (straddress1.isEmpty() ) {
            status = false;
            edtAddress1.setError(getString(R.string.address_required));
            edtAddress1.requestFocusFromTouch();
        }else if (edtAddress1.getText().length() < 3) {
            status = false;
            edtAddress1.setError(getString(R.string.valid_address_required));
            edtAddress1.requestFocusFromTouch();
        } else if (straddress2.isEmpty() ) {
            status = false;
            edtAddress2.setError(getString(R.string.address_required));
            edtAddress2.requestFocusFromTouch();
        } else if ( edtAddress2.getText().length() < 3) {
            status = false;
            edtAddress2.setError(getString(R.string.valid_address_required));
            edtAddress2.requestFocusFromTouch();
        } else if (strlandmark.isEmpty() ) {
            status = false;
            edtLandMark.setError(getString(R.string.landmark_required));
            edtLandMark.requestFocusFromTouch();
        }else if ( edtLandMark.getText().length() < 3) {
            status = false;
            edtLandMark.setError(getString(R.string.valid_landmark_required));
            edtLandMark.requestFocusFromTouch();
        }  else if (provinceListResults.isEmpty()) {
            status = false;
            Toast.makeText(context, getString(R.string.provinance_required), Toast.LENGTH_SHORT).show();
        } else if (districtListResults.isEmpty()) {
            status = false;
            Toast.makeText(context, getString(R.string.district_required), Toast.LENGTH_SHORT).show();
        } else if (mandalListResults.isEmpty()) {
            status = false;
            Toast.makeText(context, getString(R.string.mandal_required), Toast.LENGTH_SHORT).show();
        } else if (villageListResults.isEmpty()) {
            status = false;
            Toast.makeText(context, getString(R.string.village_required), Toast.LENGTH_SHORT).show();
        } else if (strpin.isEmpty()) {
            status = false;
            edtPincode.setError(getString(R.string.postalcode_required));
            edtPincode.requestFocusFromTouch();
        }
        return status;
    }

    public boolean checkLocationPermission(final Context context) {
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(context)
                        .setTitle("location")
                        .setMessage("location")
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(context,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        locationManager.requestLocationUpdates(provider, 400, 1, (LocationListener) Update_RegistrationViewFragment.this);
                    }

                } else {
                }
                return;
            }

        }
    }

    private void initMap(Bundle savedInstanceState) {

        mMapView = (MapView) view.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                googleMap.setMyLocationEnabled(true);

                LatLng sydney = new LatLng(lat, log);
                googleMap.addMarker(new MarkerOptions().position(sydney).title("Marker Title").snippet("Marker Description"));

                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });

    }

    private void getRequest(String providerType) {
        showDialog(getActivity(), getString(R.string.authenticating_alert));
        MyServices service = ServiceFactory.createRetrofitService(context, MyServices.class);
        operatorSubscription = service.getBusinessRequest(ApiConstants.BUSINESS_CAT_REQUESTS + Integer.parseInt(providerType))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BusinessCategoryModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        Toast.makeText(context, getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(BusinessCategoryModel businessCategoryModel) {
                        hideDialog();
                        businessListResults = (ArrayList<BusinessCategoryModel.ListResult>) businessCategoryModel.getListResult();
                        for (int i = 0; i < businessCategoryModel.getListResult().size(); i++) {
                            businessArrayList.add(businessCategoryModel.getListResult().get(i).getDescription());
                        }
                        Update_RegistrationViewFragment.CustomSpinnerAdapter customSpinnerAdapter = new Update_RegistrationViewFragment.CustomSpinnerAdapter(getActivity(), businessArrayList);
                        businessArrayList.add(0, getString(R.string.select_business_category));
                        spinnerBusinessCat.setAdapter(customSpinnerAdapter);
                    }

                });


    }

    private void initBusinessCatSpinner() {

        spinnerBusinessCat = (Spinner) view.findViewById(R.id.spinner_business_cat);
        spinnerBusinessCat.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event != null && event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    InputMethodManager imm = ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE));
                    boolean isKeyboardUp = imm.isAcceptingText();

                    if (isKeyboardUp)
                    {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
                return false;
            }
        }) ;
        spinnerBusinessCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
           /* case R.id.btn_personal:
                showToast(getActivity(), getString(R.string.please_Fill_The_Personal_Details));

                break;*/
            case R.id.btn_bank:
                if(getActivity().getCurrentFocus() != null)
                {
                    InputMethodManager inputManager =
                            (InputMethodManager) getActivity().
                                    getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(
                            getActivity().getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }
                if (isOnline(getActivity())) {
                    if (isValidateUi()) {

                        if(btnContinue.getText().toString().equalsIgnoreCase("continue")){
                            updatePersonalInfoNew();
                        }
                        if(is_exisisting_user){
                            updatePersonalInfo();
                        }else {
                            postPersonalInfo();
                        }


                    }
                } else {
                    showToast(getActivity(), getString(R.string.no_internet));
                }
                /*if (is_exisisting_user) {
                    replaceFragment(getActivity(), MAIN_CONTAINER, new Update_BankDetailFragment(), TAG, Update_BankDetailFragment.TAG);


                } else {
                    if (isValidateUi()) {
                        postPersonalInfo();
                       // hideKeyboard(context);
                     *//*   if(getActivity().getCurrentFocus() != null)
                        {
                            InputMethodManager inputManager =
                                    (InputMethodManager) getApplicationContext().
                                            getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputManager.hideSoftInputFromWindow(
                                    getActivity().getCurrentFocus().getWindowToken(),
                                    InputMethodManager.HIDE_NOT_ALWAYS);
                        }*//*
                       *//* InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);*//*
                    }
                }
*/
                break;
            case R.id.btn_cancel:
                replaceFragment(getActivity(), MAIN_CONTAINER, new MainFragment(), TAG, MainFragment.TAG);
                break;

        }
    }


    class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

        private final Context activity;
        private ArrayList<String> asr;

        public CustomSpinnerAdapter(Context context, ArrayList<String> asr) {
            this.asr = asr;
            activity = context;
        }


        public int getCount() {
            return asr.size();
        }

        public Object getItem(int i) {
            return asr.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }


        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView txt = new TextView(getActivity());
            txt.setPadding(16, 16, 16, 16);
            txt.setTextSize(18);
            txt.setGravity(Gravity.CENTER_VERTICAL);
            txt.setText(asr.get(position));
            txt.setTextColor(Color.parseColor("#000000"));
            return txt;
        }

        public View getView(int i, View view, ViewGroup viewgroup) {
            TextView txt = new TextView(getActivity());
            txt.setGravity(Gravity.LEFT);
            txt.setPadding(16, 16, 16, 16);
            txt.setTextSize(18);
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down24, 0);
            txt.setText(asr.get(i));
            txt.setTextColor(Color.parseColor("#000000"));
            return txt;
        }
    }


    public void ReplcaFragment(android.support.v4.app.Fragment fragment) {
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

    public String formatDateTime() {
        String date = null;
        strCurrentDate = edtDOB.getText().toString();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date newDate = format.parse(strCurrentDate);
            format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            date = format.format(newDate);
            Log.d(TAG, "formatDateTime: " + date);
            return date;
        } catch (Exception e) {
            return date;
        }

    }

    public String formatDateTimeUi() {
        String date = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            Date newDate = format.parse(dateOfBirth);
            format = new SimpleDateFormat("dd/MM/yyyy");
            date = format.format(newDate);
            Log.d(TAG, "formatDateTime: " + date);
            return date;
        } catch (Exception e) {
            return date;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}
