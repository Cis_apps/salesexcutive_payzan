package calibrage.payzanagent.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import calibrage.payzanagent.R;
import calibrage.payzanagent.activity.HomeActivity;
import calibrage.payzanagent.commonUtil.CommonUtil;
import calibrage.payzanagent.model.LoginModel;
import calibrage.payzanagent.model.LoginResponseModel;
import calibrage.payzanagent.networkservice.MyServices;
import calibrage.payzanagent.networkservice.ServiceFactory;
import calibrage.payzanagent.utils.CommonConstants;
import calibrage.payzanagent.utils.SharedPrefsData;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static calibrage.payzanagent.commonUtil.CommonUtil.EMOJI_FILTER;
import static calibrage.payzanagent.commonUtil.CommonUtil.updateResources;
import static calibrage.payzanagent.utils.CommonConstants.CLIENT_ID;
import static calibrage.payzanagent.utils.CommonConstants.CLIENT_SECRET;
import static calibrage.payzanagent.utils.CommonConstants.SCOPE;


public class LoginFragment extends BaseFragment {

    public static final String TAG = LoginFragment.class.getSimpleName();
    private Button btnLogin;
    private EditText txt_Email, txt_password;
    private LinearLayout langLyt;
    private Subscription mRegisterSubscription;
    FragmentManager fragmentManager;
    View view;
    public Toolbar toolbar;
    private Context context;
    private String mobileOrEmail, passCode, dateandtime;

    public LoginFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);

        view = inflater.inflate(R.layout.login_fragment, container, false);
        context = this.getActivity();
        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard(context);
                   // hideSoftKeyboard(LoginFragment.this);
                    return false;
                }
            });
        }


        HomeActivity.toolbar.setTitle(getResources().getString(R.string.login_sname));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !CommonUtil.areAllPermissionsAllowed(context, CommonUtil.PERMISSIONS_REQUIRED)) {
            ActivityCompat.requestPermissions(getActivity(), CommonUtil.PERMISSIONS_REQUIRED, CommonUtil.PERMISSION_CODE);
        }

        if (SharedPrefsData.getInstance(context).getIntFromSharedPrefs("lang") == 1 && CommonUtil.ISLANG) {
            // rbEng.setChecked(true);
            updateResources(context, "en-US");
            Intent i = new Intent(context.getApplicationContext(), HomeActivity.class);
            startActivity(i);
            CommonUtil.ISLANG = false;
        } else if (SharedPrefsData.getInstance(context).getIntFromSharedPrefs("lang") == 2 && CommonUtil.ISLANG) {
            //rbSim.setChecked(true);
            updateResources(context, "si");
            Intent i = new Intent(context.getApplicationContext(), HomeActivity.class);
            startActivity(i);
            CommonUtil.ISLANG = false;
        } else if (SharedPrefsData.getInstance(context).getIntFromSharedPrefs("lang") == 3 && CommonUtil.ISLANG) {
            // rbTl.setChecked(true);
            updateResources(context, "ta");
            Intent i = new Intent(context.getApplicationContext(), HomeActivity.class);
            startActivity(i);
            CommonUtil.ISLANG = false;
        }
        btnLogin = (Button) view.findViewById(R.id.btnLogin);
        txt_password = (EditText) view.findViewById(R.id.txt_password);
        txt_Email = (EditText) view.findViewById(R.id.txt_Email);
        langLyt = (LinearLayout) view.findViewById(R.id.langlyt);
        txt_password.setFilters(new InputFilter[]{EMOJI_FILTER});
        fragmentManager = getActivity().getSupportFragmentManager();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hideKeyboard(context);
                if (isOnline(getActivity())) {

                    if (isValidateUi()) {
                        login();
                        dateAndtime();
                    }

                } else {
                    showToast(getActivity(), getString(R.string.no_internet));
                }

            }
        });

        txt_Email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if (charSequence.length() > 0) {
                   // .setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                String x = "";
                x=s.toString();
                if(x.startsWith("0")){
                    txt_Email.setText("");
                }
            }
        });
        langLyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectLanguage();
            }
        });
        return view;
    }

    private void selectLanguage() {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_select_language);
        dialog.setTitle("Choose Language/භාෂාව තෝරන්න");
        RadioButton rbEng = dialog.findViewById(R.id.rbEng);
        RadioButton rbSim = dialog.findViewById(R.id.rbSim);
        RadioButton rbTl = dialog.findViewById(R.id.rbTl);

        if (SharedPrefsData.getInstance(context).getIntFromSharedPrefs("lang") == 1) {
            rbEng.setChecked(true);
            updateResources(context, "en-US");
        } else if (SharedPrefsData.getInstance(context).getIntFromSharedPrefs("lang") == 2) {
            rbSim.setChecked(true);
            updateResources(context, "si");
        } else if (SharedPrefsData.getInstance(context).getIntFromSharedPrefs("lang") == 3) {
            rbTl.setChecked(true);
            updateResources(context, "ta");
        }

        rbEng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                updateResources(context, "en-US");
                SharedPrefsData.getInstance(context).updateIntValue(context, "lang", 1);
                Intent i = new Intent(context.getApplicationContext(), HomeActivity.class);
                startActivity(i);

                dialog.dismiss();
            }
        });
        rbSim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateResources(context, "si");
                SharedPrefsData.getInstance(context).updateIntValue(context, "lang", 2);
                Intent i = new Intent(context.getApplicationContext(), HomeActivity.class);
                startActivity(i);


                dialog.dismiss();
            }
        });
        rbTl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateResources(context, "ta");
                SharedPrefsData.getInstance(context).updateIntValue(context, "lang", 3);
                Intent i = new Intent(context.getApplicationContext(), HomeActivity.class);
                startActivity(i);

                dialog.dismiss();
            }
        });

        dialog.show();
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_logout);
        item.setVisible(false);
    }

    private void dateAndtime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String formattedDate = df.format(c.getTime());
        dateandtime = formattedDate;
        SharedPrefsData.getInstance(context).updateStringValue(context, "datetime", dateandtime);
    }

    private boolean isValidateUi() {

        boolean status = true;
        mobileOrEmail = txt_Email.getText().toString().trim();
        passCode = txt_password.getText().toString();

        if (mobileOrEmail.isEmpty()) {
            txt_Email.requestFocus();
            status = false;
            //Toast.makeText(context, getString(R.string.mobileNumber_required), Toast.LENGTH_SHORT).show();
            Toast.makeText(context, getString(R.string.mobileNumber_required), Toast.LENGTH_SHORT).show();
        } else if (!isValidPhone()) {
            Toast.makeText(context, getString(R.string.valid_mobile), Toast.LENGTH_SHORT).show();
            status = false;
        } /*else if (!android.util.Patterns.PHONE.matcher(mobileOrEmail).matches()) {
            Toast.makeText(context, getString(R.string.valid_mobile), Toast.LENGTH_SHORT).show();
            status = false;
        } */else if (passCode.isEmpty()) {
            txt_password.requestFocus();
            status = false;
            Toast.makeText(context, getString(R.string.password_required), Toast.LENGTH_SHORT).show();
        }else if (txt_password.getText().length()<8) {
            txt_password.requestFocus();
            status = false;
            Toast.makeText(context, getString(R.string.valid_password_required), Toast.LENGTH_SHORT).show();
        }

        return status;
    }

    private boolean isValidPhone() {
        String target = txt_Email.getText().toString().trim();
        if (target.length() != 10) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(target).matches();
        }
    }

    private void login() {
        showDialog(getActivity(), getString(R.string.authenticating_alert));
        JsonObject object = getLoginObject();
        MyServices service = ServiceFactory.createRetrofitService(getActivity(), MyServices.class);
        mRegisterSubscription = service.UserLogin(object)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LoginResponseModel>() {
                    @Override
                    public void onCompleted() {
                        // Toast.makeText(getActivity(), "check", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        Toast.makeText(getActivity(), getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(LoginResponseModel loginResponseModel) {
                        hideDialog();
                        if (loginResponseModel.getIsSuccess()) {
                            String Token = loginResponseModel.getResult().getAccessToken();
                            // CommonConstants.USERID = loginResponseModel.getResult().getUser().getId();
                            CommonConstants.USER_NAME = loginResponseModel.getResult().getUser().getUserName();
                            SharedPrefsData.getInstance(getActivity()).updateIntValue(getActivity(), CommonConstants.ISLOGIN, CommonConstants.Login);
                            SharedPrefsData.getInstance(getActivity()).saveUserId(getActivity(), loginResponseModel.getResult().getUser().getId());
                            SharedPrefsData.getInstance(context).updateStringValue(context, "Token", loginResponseModel.getResult().getTokenType() + " " + Token);

                            SharedPrefsData.getInstance(context).updateStringValue(context, "RefreshToken", loginResponseModel.getResult().getRefreshToken());
                            SharedPrefsData.getInstance(context).updateStringValue(context, "userid", loginResponseModel.getResult().getUser().getId());
                            SharedPrefsData.getInstance(context).updateStringValue(context, "username", loginResponseModel.getResult().getUser().getUserName());
                            SharedPrefsData.getInstance(getActivity()).saveUserName(getActivity(), loginResponseModel.getResult().getUser().getUserName());
                            SharedPrefsData.getInstance(context).updateStringValue(context, "Token", loginResponseModel.getResult().getTokenType() + " " + loginResponseModel.getResult().getAccessToken());
                            loginFragment(getActivity(), MAIN_CONTAINER, new MainFragment(), TAG, MainFragment.TAG);
                        } else {
                            showToast(context, loginResponseModel.getEndUserMessage());
                        }

                    }
                });

    }


    private JsonObject getLoginObject() {
        LoginModel loginModel = new LoginModel();
        loginModel.setPassword(txt_password.getText().toString());
        loginModel.setUserName(txt_Email.getText().toString());
        loginModel.setClientId(CLIENT_ID);
        loginModel.setClientSecret(CLIENT_SECRET);
        loginModel.setScope(SCOPE);
        return new Gson().toJsonTree(loginModel)
                .getAsJsonObject();
    }

    private void hideSoftKeyboard(LoginFragment loginFragment) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) getActivity().getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                getActivity().getCurrentFocus().getWindowToken(), 0);

    }

    public void ReplcaFragment(android.support.v4.app.Fragment fragment) {

        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }


}
