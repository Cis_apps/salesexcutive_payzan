package calibrage.payzanagent.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import calibrage.payzanagent.R;
import calibrage.payzanagent.adapter.InProgressRequetAdapter;
import calibrage.payzanagent.interfaces.RequestClickListiner;
import calibrage.payzanagent.model.InProgressPostModel;
import calibrage.payzanagent.model.InProgressResponseModel;
import calibrage.payzanagent.networkservice.MyServices;
import calibrage.payzanagent.networkservice.ServiceFactory;
import calibrage.payzanagent.utils.CommonConstants;
import calibrage.payzanagent.utils.EndlessRecyclerOnScrollListener;
import calibrage.payzanagent.utils.SharedPrefsData;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class InProgressFragment extends BaseFragment implements RequestClickListiner {

    public static final String TAG = InProgressFragment.class.getSimpleName();

    View view;
    public RecyclerView recyclerView;
    FragmentManager fragmentManager;
    private Context context;
    TextView noRecords;
    private Subscription operatorSubscription;
    private ArrayList<InProgressResponseModel.Result.AgentRequestDetail> listResults = new ArrayList<>();
    private InProgressResponseModel agentRequestModelBundle;
    public static Toolbar toolbar;
    private EditText search;
    ImageView imageView;
    private InProgressPostModel inprogressInfo;
    private InProgressRequetAdapter inProgressRequetAdapter;
    private List<String> list = new ArrayList<String>();
    private String searchStr = "";
    int Index = 0, pazeSize = 10, totalItemCount, lastVisibleItem;
    LinearLayoutManager layoutManager;
    // private boolean loading = false;
    private SwipeRefreshLayout swipeToRefresh;
    int CurrentCount = 10;


    public InProgressFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_in_progress, container, false);
        inprogressInfo = new InProgressPostModel();
        context = this.getActivity();
        hideKeyboard(context);
        recyclerView = view.findViewById(R.id.recylerview_card_inprogress);
        noRecords = view.findViewById(R.id.no_records);
        imageView = (ImageView) view.findViewById(R.id.iv_clear);
        search = (EditText) view.findViewById(R.id.search);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        fragmentManager = getActivity().getSupportFragmentManager();
        swipeToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                listResults.clear();
                if (!listResults.isEmpty())
                    listResults.clear();//The list for update recycle view
                inProgressRequetAdapter.notifyDataSetChanged();

                Index = 0;
                CurrentCount = 10;
                EndlessRecyclerOnScrollListener.current_page = 1;
                EndlessRecyclerOnScrollListener.loading = true;
                EndlessRecyclerOnScrollListener.previousTotal = 0;
                postList(searchStr, Index, pazeSize, true);
            }


        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!search.getText().toString().trim().equalsIgnoreCase("")) {
                    search.setText("");
                    listResults.clear();
                    postList("", 0, 10, true);

                }
            }
        });
        if (isOnline(getActivity())) {
            postList(searchStr, Index, pazeSize, true);
        } else {
            showToast(getActivity(), getString(R.string.no_internet));
        }
        addTextListener();


        recyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener(
                layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                // do something...
                int count = 0;
                Index = Index + 1;
                EndlessRecyclerOnScrollListener.current_page = 1;
                EndlessRecyclerOnScrollListener.loading = true;
                EndlessRecyclerOnScrollListener.previousTotal = 0;
                if (CurrentCount == pazeSize) {
                    postList("", Index, pazeSize, false);
                }

            }

        });
        return view;
    }


    /**
     * @param searchStr search string
     * @param index     count
     * @param pageSize  pagesize
     * @param loaData   is used for notify recylerview is data added in pagination or new data
     */

    private void postList(final String searchStr, final Integer index, final Integer pageSize, final Boolean loaData) {
        showDialog(getActivity(), getString(R.string.authenticating_alert));
        JsonObject object = inProgressDetails(searchStr, index, pageSize);
        MyServices service = ServiceFactory.createRetrofitService(getActivity(), MyServices.class);
        operatorSubscription = service.postInprogress(object)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<InProgressResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (e instanceof HttpException) {
                            ((HttpException) e).code();
                            ((HttpException) e).message();
                            ((HttpException) e).response().errorBody();
                            try {
                                ((HttpException) e).response().errorBody().string();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        Toast.makeText(getActivity(), getString(R.string.fail_response), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(final InProgressResponseModel inProgressResponseModel) {
                        hideDialog();
                        CurrentCount = inProgressResponseModel.getResult().getPageSize();
                        Log.d(TAG, "response " + listResults.size());
//                        Log.d(TAG, "response " + inProgressResponseModel.getResult().getTotalRecords());
                        if (inProgressResponseModel.getIsSuccess()) {
                            swipeToRefresh.setRefreshing(false);
                            hideDialog();
                            Index = inProgressResponseModel.getResult().getPageIndex();
                            for (int i = 0; i < inProgressResponseModel.getResult().getAgentRequestDetails().size(); i++) {
                                listResults.add(inProgressResponseModel.getResult().getAgentRequestDetails().get(i));
                            }
                            if (listResults.isEmpty()) {
                                noRecords.setVisibility(View.VISIBLE);
                            } else {
                                noRecords.setVisibility(View.GONE);
                            }
                            if (loaData) {
                                inProgressRequetAdapter = new InProgressRequetAdapter(context, listResults);
                                recyclerView.setAdapter(inProgressRequetAdapter);
                                recyclerView.setHasFixedSize(true);
                                inProgressRequetAdapter.notifyDataSetChanged();
                                inProgressRequetAdapter.setOnAdapterListener(InProgressFragment.this);
                            } else {
                                inProgressRequetAdapter.notifyDataSetChanged();
                            }
                            //agentRequestModelBundle = listResults;
                            for (int i = 0; i < listResults.size(); i++) {
                                list.add(listResults.get(i).getMobileNumber());
                            }
                        } else {
                            showToast(context, inProgressResponseModel.getEndUserMessage());
                        }

                    }
                });
    }

    private JsonObject inProgressDetails(String searchStr, Integer index, Integer pageSize) {
        inprogressInfo.setUserId(SharedPrefsData.getInstance(context).getUserId(context));
        inprogressInfo.setStatusTypeIds(CommonConstants.STATUSTYPE_ID_IN_PROGRESS + "," + CommonConstants.STATUSTYPE_ID_SUBMIT_FOR_REVIEW);
        if (searchStr != null && searchStr.equalsIgnoreCase("")) {
            inprogressInfo.setSearchItem(searchStr);
            inprogressInfo.setIndex(index);
            inprogressInfo.setPageSize(pageSize);
        } else {
            inprogressInfo.setSearchItem(searchStr);
            inprogressInfo.setIndex(null);
            inprogressInfo.setPageSize(null);
        }

        Log.e("inprogressInfo", "" + new Gson().toJsonTree(inprogressInfo)
                .getAsJsonObject());
        return new Gson().toJsonTree(inprogressInfo)
                .getAsJsonObject();

    }

    public void addTextListener() {
        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (v.getText().toString().equalsIgnoreCase("") || v.getText().toString().equalsIgnoreCase(null)) {
                        listResults.clear();
                        postList(" ", null, null, false);

                    } else {
                        listResults.clear();
                        postList(v.getText().toString().trim(), null, null, false);
                    }
                    return true;
                }
                return false;
            }
        });
    }

    public void ReplcaFragment(android.support.v4.app.Fragment fragment) {
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }


    @Override
    public void onAdapterClickListiner(int pos, boolean b) {
        Bundle bundle = new Bundle();
       // bundle.putParcelable("request", agentRequestModelBundle);
        bundle.putInt("position", pos);

        bundle.putString("intAgentRequestId",""+listResults.get(pos).getId());
        bundle.putString("edtFirstName",listResults.get(pos).getFirstName());
        bundle.putString("edtMiddleName",listResults.get(pos).getMiddleName());
        bundle.putString("edtLastName",listResults.get(pos).getLastName());
        bundle.putString("edtMobile",listResults.get(pos).getMobileNumber());
        bundle.putString("strbundleMobile",listResults.get(pos).getMobileNumber());
        bundle.putString("edtEmail",listResults.get(pos).getEmail());
        bundle.putString("edtAddress1",listResults.get(pos).getAddressLine1());
        bundle.putString("edtAddress2",listResults.get(pos).getAddressLine2());
        bundle.putString("edtLandmark",listResults.get(pos).getLandmark());
        bundle.putString("strtitleType",listResults.get(pos).getTitleType());
      //  Log.d(TAG, "onAdapterClickListiner: "+bundle.toString());
        Fragment fragment = new Update_RegistrationViewFragment();
        fragment.setArguments(bundle);
        CommonConstants.MOBILE_NUMBER = listResults.get(pos).getMobileNumber();
        CommonConstants.Is_New_Agent_Request = false;
        CommonConstants.AGENT_REQUEST_ID = listResults.get(pos).getId().toString();
        replaceFragment(getActivity(), MAIN_CONTAINER, fragment, TAG, Update_RegistrationViewFragment.TAG);

    }


}
