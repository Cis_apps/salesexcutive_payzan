package calibrage.payzanagent.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 12/29/2017.
 */

public class InProgressPostModel {

    @SerializedName("UserId")
    @Expose
    private String userId;
    @SerializedName("StatusTypeIds")
    @Expose
    private String statusTypeIds;
    @SerializedName("SearchItem")
    @Expose
    private String searchItem;
    @SerializedName("Index")
    @Expose
    private Integer index;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStatusTypeIds() {
        return statusTypeIds;
    }

    public void setStatusTypeIds(String statusTypeIds) {
        this.statusTypeIds = statusTypeIds;
    }

    public String getSearchItem() {
        return searchItem;
    }

    public void setSearchItem(String searchItem) {
        this.searchItem = searchItem;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
