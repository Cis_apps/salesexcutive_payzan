package calibrage.payzanagent.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import calibrage.payzanagent.R;
import calibrage.payzanagent.model.AgentRequestModel;


public class GenericAdapter<T> extends ArrayAdapter<T> {

    private Context context;
    private int resource;
    private ArrayList<AgentRequestModel.ListResult> data;
    private AdapterOnClick adapterOnClick;


    public GenericAdapter(Context context, List<T> tList, int res) {
        super(context, res, tList);
        data = (ArrayList<AgentRequestModel.ListResult>) tList;
        this.context = context;
        resource = res;

    }

    @Override
    public int getCount() {
        return data.size();
    }


    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        try {
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                convertView = inflater.inflate(resource, viewGroup, false);
            }

            TextView name = (TextView) convertView.findViewById(R.id.tracking_list_agentname);
            name.setText(data.get(i).getAgentRequestCategoryName());
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapterOnClick.adapterOnClick(i);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    public interface AdapterOnClick {
        void adapterOnClick(int position);
    }

    public void setAdapterOnClick(AdapterOnClick adapterOnClick) {
        this.adapterOnClick = adapterOnClick;
    }
}
